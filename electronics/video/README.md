# Text Video Adapter

## Text Video Adapter Connector

Before anything else we are taking a look at the connector that connects the MPU and the video boards.
The signals defined here are making an appearance in the schematics.
Therefore it is helpful to first see and understand the control signals communicated over this connector.

| Pin | Signal | Description |
| --: | ------ | ----------- |
|   1 | `VCC` | Powers the board with +5V. Its all (vintage) 5V chips. |
|   2 | `!VRAM CE` | Chip enable for the video RAM. Active low. The MPU pulls this low if it wants to access the video memory. |
|   3 | `R/!W` | 6800 style read/write signal. High if MPU wants to read fron video RAM and low if MPU wants to write to video RAM. |
|   4 | `!VRAM busy` | The video RAM may indicate access conflicts. Active low. This should however never happen as the MPU is reading and writing but the CRTC is reading only. |
|   5 | `A10` | MPU address bit 10 (most significant bit). This is the most significant bit, as we are not utilizing the full 16 bit address range of the MPU. The video Memory is only 2kB and the CRTC only has 40 registers. |
|   6 | `!VRAM OE` | This enables or disables the video RAM output buffers. Active low. If disabled then all (data) output pins are in high impedance state. This is somewhat redundand to the `!VRAM CE` signal. One can leave this always low/active and control the video RAM with `!VRAM CE` and `R/!W`. |
|   7 | `A0` | MPU address bit 0 (least significant bit). |
|   8 | `A1` | MPU address bit 1. |
|   9 | `A2` | MPU address bit 2. |
|  10 | `A3` | MPU address bit 3. |
|  11 | `A4` | MPU address bit 4. |
|  12 | `A5` | MPU address bit 5. |
|  13 | `A6` | MPU address bit 6. |
|  14 | `A7` | MPU address bit 7. |
|  15 | `A8` | MPU address bit 8. |
|  16 | `A9` | MPU address bit 9. |
|  17 | `D0` | MPU data bit 0 (least significant bit). |
|  18 | `D1` | MPU data bit 1. |
|  19 | `D2` | MPU data bit 2. |
|  20 | `D3` | MPU data bit 3. |
|  21 | `D4` | MPU data bit 4. |
|  22 | `D5` | MPU data bit 5. |
|  23 | `D6` | MPU data bit 6. |
|  24 | `D7` | MPU data bit 7 (most significant bit). |
|  25 | `GND` | 0V. |
|  26 | `!CRTC CS` | CRTC chip select. Active low. The MPU pulls this low if it wants to read from or write to CRTC regisers. |
|  27 | `!RD` | 8080 style read signal. Active low. The MPU pulls this low if it wants to read from CRTC registers. |
|  28 | `!WR` | 8080 style write signal. Active low. The MPU pulls this low if it wants to write to CRTC registers. |
|  29 | `!Reset` | Resets the CRTC into its initial state. Active low. This is the global reset signal that also resets the MPU and all other chips. |
|  30 | `` |  |
|  31 | `Font sel1` | Font selection bit 1 (most significant.) |
|  32 | `Font sel0` | Font selection bit 0 (least significant.) |

## Schematics

This is the wiring diagram for the text video adapter.
Its various components are explained in the following paragraphs.

![text video adapter schematics](media/schematics.png "Text Video Adapter Schematics")

## Clock Generator

TODO

## Video RAM

TODO

## Character ROM

The character ROM is a 27128 type of UV EPROM.
It holds a total of 16kB of binary font data.

Each character is made up of 8x16 pixels.
Each font holds 256 of these characters.
That accounts for each font being 4kB in size.

As a result the 27128ish ROM holds 4 fonts to be selected from.

## Selecting A Font

The signals `Font sel0` and `Font sel1` are forming kind of a small address bus.
These signals are directly wired to the character ROM.
The signals are selecting 1 out of 4 fonts available on the ROM chip.

| `Font sel1` | `Font sel0` | Font number | Font |
| :---------: | :---------: | :---------: | ---- |
| 0           | 0           | 0           | A font with serifs and fine lines (one pixel). |
| 0           | 1           | 1           | A font without serifs and thick lines (two pixels). |
| 1           | 0           | 2           | An artistic computer font. More stylish but less readable. |
| 1           | 1           | 3           | An artistic, coarse font. More stylish but less readable. |

Here are examples for how those fonts are looking.

### Font #0 - Holger's Serif

![holgers serif](media/holgers_serif.png "Holger's Serif")

### Font #1 - Holger's Serif

![holgers sans serif](media/holgers_sans_serif.png "Holger's Sans Serif")

### Font #2 - Holger's Computer

![holgers computer](media/holgers_computer.png "Holger's Computer")

### Font #3 - Holger's Template

![holgers template](media/holgers_template.png "Holger's Template")
