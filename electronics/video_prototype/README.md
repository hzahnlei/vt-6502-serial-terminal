# Prototypical Text Video Adapter

At first I was constructing a prototypical text video adapter based on the HD6445 CRTC-II that I had salvaged from the Brother WP-1.
I needed a prototype in order to learn and find out how things work as this is my first video circuit based on a 6845-like chip.

## Clock Generation

A 74LS163 synchronous counter is used to generate various clocks.
These are all derived from the 15.33MHz pixel clock (`Dot clk`) used to clock the counter IC.

`Q0`- `Q3` are the counter bits where `Q0` is least significant.
The counter IC is configured so that `0b0000` gets loaded initially.
It is reset when `Q3` goes high.
So it always counts from 0 to 8 which is 9 clock periods.
`Q2` is used as the character clock `Char clk`.
`Q3` is inverted and called `!SR load` from here on.

|       |   | `Q3` | `Q2` | `Q1` | `Q0` |   | `Char clk` | `!SR load` |
| ----: | - | ---: | ---: | ---: | ---: | - | ---------: | ---------: |
|     1 |   |    0 |    0 |    0 |    0 |   |          0 |          1 |
|     2 |   |    0 |    0 |    0 |    1 |   |          0 |          1 |
|     3 |   |    0 |    0 |    1 |    0 |   |          0 |          1 |
|     4 |   |    0 |    0 |    1 |    1 |   |          0 |          1 |
|     5 |   |    0 |    1 |    0 |    0 |   |          1 |          1 |
|     6 |   |    0 |    1 |    0 |    1 |   |          1 |          1 |
|     7 |   |    0 |    1 |    1 |    0 |   |          1 |          1 |
|     8 |   |    0 |    1 |    1 |    1 |   |          1 |          1 |
|     9 |   |    1 |    0 |    0 |    0 |   |          1 |          0 |

The following circuit diagram shows the clock generation as well as some analogue circuitry to condition the video signal before sending it of to the CRT drive board salvaged from the WP-1.

Take a look at the circuit diagrams provided to understand what `Char clk` and `!SR load` are used for.

![video clock circuitry](media/video_schematic_1of2.png "Video Clock Circuitry"){width=50%}

## Decoupling MPU and CRTC Busses

MPU and CRTC both need access to the video RAM.
To prevent bus clashes, MPU and CRTC accesses are usually synchronized so that they never access the VRAM at the same time.
This usually means that the CRTC has precedence over the MPU.
Accesses are synchronized like on the Commodore 64, where 6502 MPU and 6567/6569 VIC-II are accessing the VRAM at diffenent phases of the clock signal.
Or, cycles are stolen from the MPU, or it is even completely halted for some short time.
The Atari 8-bit line of computers are an example for this solution where the ANTIC video chip halts the 6502C MPU.
Busses are electrically decoupled by using bus buffers with tri-state outputs such as 74LS245 etc.

In any case this requires some effort and increases chip count.
As a lazy guy, I want to reduce chip count and effort.
Therefore I am using a dual ported RAM as a video RAM.
The CRTC only reads from it.
The MPU mostly writes to it.
It may, however, read from it as well.
With this approach I am getting rid of the bus buffers.
And, I do not need to tightly synchronize MPU and CRTC.

The following circuit diagram illustrates the rest of the video circuit.
It is made up of a dual port RAM shared between MPU und CRTC as well as a character ROM and a serial shift register.

![video circuitry](media/video_schematic_2of2.png "Video Circuitry"){width=50%}

## Omitting the Pixel Output Gate

The pixel stream is mingled with the `CURSOR` and `DISP EN` signals to form the "effective pixel on/off" signal.
Originally, that signal was supposed to be gated by a 74LS74 D-type flip-flop.
This gate is supposed to guarantee a stable pixel value.
The effective pixel is stored by the flip-flop at the falling edge of the `Dot clk` and should have assumed a stable value by that time.
This was an idea I took from [AN-0851](../datasheets/AN-0851:%20Motorola%20MC6845%20CRTC%20Simplifies%20Video%20Display%20Controllers.pdf).

![circuit with output gate](media/using%20an%20output%20gate.png "Circuit With Output Gate"){width=50%}

However, the video signal was quite unstable.
More so than I had anticipated.
Please note missing pixels in the picture below.

![distorted video with output gate](media/distorted_video_with_gate.png "Distorted Video With Output Gate"){width=50%}

For curiosity I was bypassing the 74LS74 flip-flop, instead rouing the effective video signal directly to the output buffer.
This is depicted by the schematic diagram below.

![circuit without output gate](media/omitting%20the%20output%20gate.png "Circuit Without Output Gate"){width=50%}

Furthermore I set the serial input (`DS`) of the 74LS165 shift register to `VCC` (5V).
As a result one can observe a fine, thin line at the beginning of each character.

![no output gate, ds connected to vcc](media/no_gate_ds_to_vcc.png "No Output Gate, DS connected to VCC"){width=50%}

On the other hand fine gaps become visible where neighbouring characters are supposted to "touch" each other once `DS` is connected to `GND` instead of `VCC`, as can be seen next.

![no output gate, ds connected to gnd](media/no_gate_ds_to_gnd.png "No Output Gate, DS connected to GND"){width=50%}

Neither of these effects is desired.
So, finaly I connected the least significant bit of the character row (bit 0) to `DS` as well as `D0` inputs of the shift register.
To my own surprise this resulted in a more stable and clear picture even though not using the gate.
So, the output gate from [AN-0851](../datasheets/AN-0851:%20Motorola%20MC6845%20CRTC%20Simplifies%20Video%20Display%20Controllers.pdf) did not make it to my final circuit design reducing chip count as a welcome side effect.

## Resume

### Things That Work

Actually, the prototypical text video adapter draws a surprisingly stable and sharp picture on the screen, given that it is a mess of wires on a prototype board.
The picture is made up of 80x15 characters.
Each character is made up of 9x16 pixels.

![mess of wires](media/mess_of_wires.png "Mess of Wires"){width=50%}

### Things That Do Not Work

Originally, I planned for having 91x15 characters, just as the original WP-1.
However, It seems that I had picked the wrong shift register (74LS165).
`!SR load`, which is used load the shift register, is active at the same time as `Dot clk`, which is used to shift pixels (or dots).

Even though `Dot clk` is asserted with `!SR load` the content is not shifted.
Only with the next `Dot clk` `!SR load` becomes inactive and the first shift happens.
So I need 9 transitions of `Dot clk` instead of 8 to shift out all pixels.
As a result the leftmost pixel gets displayed twice.
And I had to change the initialization of the HD6845 to only display 80 characters per row in opposite to the possible 91 as the WP-1 does.

In the final circuit I will try a 74LS166 shift register.
There, `Dot clk` is already performing a shift, even though `!SR load` might be active.

![messy desk](media/messy_desk.png "Messy Desk"){width=50%}
