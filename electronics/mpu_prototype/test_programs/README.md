# About These Test Programs

## Target

The target for this program is the R6501 microcontroller (MCU for short).
The R6501 is based on the infamous MOS 6502 microprocessor (MPU for short).
It offers parallel I/O, timers, a serial port and RAM besides the 6502 MPU core.
This said, the MCU defines a memory map which we need to keep in mind.
Addresses for hardware vectors are predetermined by the 6502 MPU core.
Addresses for I/O control registers are predetermined by the R6501 MCU.

For details on the R6501 see [R6501 One Chip Microprocessor](../../../datasheets/R6501.pdf) datasheet.

## Memory Map

- I decided to have the __video memory__ (aka frame buffer) starting at 0x0000.
  Hence video RAM and R6501's built-in RAM overlap by a few bytes.
  (Few by todays standards.)
  This needs to be kept in mind when configuring the CRTC.
  The CRTC features a register for off-setting the start of the video memory used.
- An 8k ROM is attached to the R6501 MCU holding __program and hardware vectors__.
  The ROM is mapped to 0xE000 as MCU architecture requires the vectors to reside at the very end of the 64k address space.
  Hence, the program starts at 0xE000.

## Cross Assembler

All assembly codes herein are written for the SBASM cross assembler. 
The `Makefile` expects the SBASM executable to be in the PATH.
SBASM is written in Python. 
Hence Python needs to be installed and is expected to be in the PATH too.

For details on the SB-Assembler see: https://www.sbprojects.net/sbasm/index.php
