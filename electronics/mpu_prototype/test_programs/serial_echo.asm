;==============================================================================
; Toggle all bits of digital output port C, so that we can see whether the cir-
; cuit is working or not.  Observing the blinking port can be achieved by means
; of an ordinary voltmeter or by attaching an LED to PC0.
; This variant of a "blinking" program receives bytes over the serial port and
; echoes them back to the sender.
; The Baud rate generator is configured for 9.600 bps under the assumption that
; the MCU clock speed is 1.8432MHz.
; Set your serial terminal to 8N2@9600 bps to communicate with this serial echo
; "server". However, 8N1@9600 does work as well.
;
; (c) 2024 by Holger Zahnleiter, all rights reserved
;==============================================================================

	.cr 6502
	.tf serial_echo.hex, int

;------------------------------------------------------------------------------
; Definitions
;------------------------------------------------------------------------------

; Addresses of R6501 hardware control registers
PORT_C    = 0x0002	; I/O Port C

IER       = 0x0012	; Interrupt Enable Register

MCR       = 0x0014	; Mode Control Register

SCCR      = 0x0015	; Serial Communication Control Register
SCSR      = 0x0016	; Serial Communication Status Register
SCSR_RDRF = 0b00000001	; SCSR flag Receiver Data Register Full
SCSR_TDRE = 0b01000000  ; SCSR flag Transmitter Data Register Empty
SCDAT     = 0x0017	; Serial Communication Data Register

LLA       = 0x0018	; Lower Latch counter A
ULA       = 0x0019	; Upper Latch counter A 

;------------------------------------------------------------------------------
; Program
;------------------------------------------------------------------------------

	.org 0xE000
MAIN:
	sei			; Disable all interrupts globally

	cld			; Use binary instead of BCD arithmetic

	ldx #0xff		; Stackpointer points to 0xFF and grows towards
	txs			; 0x00.

	lda #0b00000000		; Disable all interrupts individually
	sta IER
	
	lda #0b01000000		; Select "normal" bus mode, both timers in
	sta MCR			; interval mode

	lda #0b11000000		; Enable XMTR/RCVR, async, 8N1(actually it is
	sta SCCR		; 8N2 but communication partners may use 8N1)

	lda #0x00		; Counter A (frequency divider) latch (re-load 
	sta ULA			; value) set to 0x000B for standard Baud rate
	lda #0x0B		; of 9.600 bps (assuming MCU clocked at
	sta LLA			; 1.8432MHz)

.LOOP
	lda #0b11111111		; Keep all port pins high for some time
	sta PORT_C		; while also listening for bytes to arrive
	jsr DELAY_AND_ECHO	; at serial port

	lda #0b00000000		; Keep all port pins low for some time
	sta PORT_C		; while also listening for bytes to arrive
	jsr DELAY_AND_ECHO	; at serial port

	jmp .LOOP		; Repeat the above to make port pins toggle
				; periodically

;------------------------------------------------------------------------------
; Subroutines
;------------------------------------------------------------------------------

DELAY_AND_ECHO:
	ldx #0xff	; Set counters for busy-waiting style delay
	ldy #0xff	; 256×256=65.536 repetitions in total
.TRY_RECEIVE
	lda SCSR	; Check if byte received
	and #SCSR_RDRF
	beq .NOTHING_TO_ECHO
	lda SCDAT	; Read byte received (also clears SCSR_RDRF)
	pha
.WAIT_TX_READY	
	lda SCSR	; Check if transmit buffer empty/ready
	and #SCSR_TDRE 	
	beq .WAIT_TX_READY
	pla		; Echo back byte received before
	sta SCDAT
.NOTHING_TO_ECHO
	dex		; Inner delay loop
        bne .TRY_RECEIVE
        dey		; Outer delay loop
        bne .TRY_RECEIVE
        rts
	
;------------------------------------------------------------------------------
; Interrupt service routines
;------------------------------------------------------------------------------

NMI_ISR:
	rti

IRQ_BRK_ISR:
	rti

;------------------------------------------------------------------------------
; Hardware vectors
;------------------------------------------------------------------------------
		
	.org 0xfffa
	.dw NMI_ISR	; NMI vector
	.dw MAIN	; RESET vector
	.dw IRQ_BRK_ISR	; IRQ and BRK vector

	.end
