;==============================================================================
; Toggle all bits of digital output port C, so that we can see whether the cir-
; cuit is working or not.  Observing the blinking port can be achieved by means
; of an ordinary voltmeter or by attaching an LED to PC0.
; This variant of a "blinking" program helps testing the keyboard circuitry. It
; sets the signals  KbRow1 and KbCol1 to logic heigh (+5V),  wherease all other
; KbRow* and KbCol*  are set to logic low (0V).  By pressing the "T" key on the
; keyboard the  kbIn0 signal gets pulled from high to low.  This influences the
; blinking frequency of the LED. When pressed it should blink noticeably faster
; compared to when released.
;
; (c) 2024 by Holger Zahnleiter, all rights reserved
;==============================================================================

	.cr 6502
	.tf read_key.hex, int

;------------------------------------------------------------------------------
; Definitions
;------------------------------------------------------------------------------

; Addresses of R6501 hardware control registers
PORT_B = 0x0001	; I/O Port C
PORT_C = 0x0002	; I/O Port C
PORT_D = 0x0003	; I/O Port D
MCR    = 0x0014	; Mode Control Register

;------------------------------------------------------------------------------
; Program
;------------------------------------------------------------------------------

	.org 0xE000
MAIN:
	sei			; Disable all interrupts globally

	cld			; Use binary instead of BCD arithmetic

	ldx #0xff		; Stackpointer points to 0xFF and grows towards
	txs			; 0x00.
	
	lda #0b01100000		; Select "normal" bus mode, port D all outputs
	sta MCR

	; This did not work when setting all pins to logic low (0)! The behavi-
	; our of  the R6501's I/O ports  deviates from  what I am used to  from
	; 6522  and similar I/O chips.  The R6501 does not know  data direction
	; registers.
	lda #0b11111111		; Port B all inputs that need to be pulled down
	sta PORT_B

	lda #0b00010010		; Set KbRow1 and KbCol1 high ("T" key)
	sta PORT_D

.LOOP
	lda #0b11111111		; Keep all port pins high for some time
	sta PORT_C
	ldy #0xff		; Long delay default ("T" not pressed)
	lda PORT_B		; Read "T" key
	and #0b00000001		; Short delay if "T" pressed
	bne .DELAY_HIGH
	ldy #0x80
.DELAY_HIGH
	jsr DELAY

	lda #0b00000000		; Keep all port pins low for some time
	sta PORT_C
	ldy #0xff		; Long delay default ("T" not pressed)
	lda PORT_B		; Read "T" key
	and #0b00000001		; Short delay if "T" pressed
	bne .DELAY_LOW
	ldy #0x80
.DELAY_LOW
	jsr DELAY

	jmp .LOOP		; Repeat the above to make port pins toggle
				; periodically

;------------------------------------------------------------------------------
; Subroutines
;------------------------------------------------------------------------------

; Expect Y register  to be set to a reasonable value  in order to determine the
; actual delay length.
DELAY:
	ldx #0xff
.LOOP
        dex
        bne .LOOP
        dey
        bne .LOOP
        rts
	
;------------------------------------------------------------------------------
; Interrupt service routines
;------------------------------------------------------------------------------

NMI_ISR:
	rti

IRQ_BRK_ISR:
	rti

;------------------------------------------------------------------------------
; Hardware vectors
;------------------------------------------------------------------------------
		
	.org 0xfffa
	.dw NMI_ISR	; NMI vector
	.dw MAIN	; RESET vector
	.dw IRQ_BRK_ISR	; IRQ and BRK vector

	.end
