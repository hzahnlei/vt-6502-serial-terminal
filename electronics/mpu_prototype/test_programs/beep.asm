;==============================================================================
; Toggle all bits of digital output port C, so that we can see whether the cir-
; cuit is working or not.  Observing the blinking port can be achieved by means
; of an ordinary voltmeter or by attaching an LED to PC0.
; This variant of a "blinking" program helps testing the keyboard circuitry. It
; sets the signals  KbRow1 and KbCol1 to logic heigh (+5V),  wherease all other
; KbRow* and KbCol*  are set to logic low (0V).  By pressing the "T" key on the
; keyboard the  kbIn0 signal gets pulled from high to low.  This influences the
; blinking frequency of the LED. When pressed it should blink noticeably faster
; compared to when released.
;
; (c) 2024 by Holger Zahnleiter, all rights reserved
;==============================================================================

	.cr 6502
	.tf beep.hex, int

;------------------------------------------------------------------------------
; Definitions
;------------------------------------------------------------------------------

; Addresses of R6501 hardware control registers
PORT_C = 0x0002	; I/O Port C
MCR    = 0x0014	; Mode Control Register
LLBC   = 0x001C ; Lower Latch B/C
ULC    = 0x001D ; Upper Latch C
ULB    = 0x001E ; Upper Latch B

;------------------------------------------------------------------------------
; Program
;------------------------------------------------------------------------------

	.org 0xE000
MAIN:
	sei			; Disable all interrupts globally

	cld			; Use binary instead of BCD arithmetic

	ldx #0xff		; Stackpointer points to 0xFF and grows towards
	txs			; 0x00.
	
	lda #0b01000100		; Select "normal" bus mode, timer B in 
	sta MCR			; Assymetrical Pulse Generation Mode outputting
				; at Port A5 (CNTB)

	; Counter B  initialization sequence  for Assymetrical Pulse Generation
	; Mode as described in the datasheet (p. 3-122).
	lda #0x00		; Pulse width P
	sta LLBC
	lda #0x08
	sta ULC
	lda #0x0		; Duration D
	sta LLBC
	lda #0x08
	sta ULB

.LOOP
	lda #0b11111111		; Keep all port pins high for some time
	sta PORT_C
	jsr DELAY

	lda #0b00000000		; Keep all port pins low for some time
	sta PORT_C
	jsr DELAY

	lda #0b01000000		; Select "normal" bus mode, timer B in Interval
	sta MCR			; Timer Mode outputting nothing at Port A5
				; (CNTB), effectively stopping to beep

	lda #0b11111111		; Keep all port pins high for some time
	sta PORT_C
	jsr DELAY

	lda #0b00000000		; Keep all port pins low for some time
	sta PORT_C
	jsr DELAY

	lda #0b01000100		; Select "normal" bus mode, timer B in 
	sta MCR			; Assymetrical Pulse Generation Mode outputting
				; at Port A5 (CNTB)

	jmp .LOOP		; Repeat the above to make port pins toggle
				; periodically

;------------------------------------------------------------------------------
; Subroutines
;------------------------------------------------------------------------------

; Expect Y register  to be set to a reasonable value  in order to determine the
; actual delay length.
DELAY:
	ldy #0xff
	ldx #0xff
.LOOP
        dex
        bne .LOOP
        dey
        bne .LOOP
        rts
	
;------------------------------------------------------------------------------
; Interrupt service routines
;------------------------------------------------------------------------------

NMI_ISR:
	rti

IRQ_BRK_ISR:
	rti

;------------------------------------------------------------------------------
; Hardware vectors
;------------------------------------------------------------------------------
		
	.org 0xfffa
	.dw NMI_ISR	; NMI vector
	.dw MAIN	; RESET vector
	.dw IRQ_BRK_ISR	; IRQ and BRK vector

	.end
