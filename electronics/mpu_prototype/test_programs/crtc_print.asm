;==============================================================================
; Initialize  CRTC and clear framebuffer.  Then populate the first 256 bytes of
; the framebuffer buffer with  character codes 0 - 256 so that we get to see at
; least something.
; An LED is attached to port PC0 on the prototype board.  This LED should blink
; once the CRTC is initialized,  so that we can see whether the  program really
; runs, in case the screen does not display anything.
; The video RAM is mapped to 0x0000 - 0x0800, that is 2kB of RAM.  So the first
; 256 bytes of the video memory are  not accessible as they are eclipsed by the
; R6501's internal memory and registers.  Therefore the HD6445 is configured to
; assume video memory  starting at 0x0100.  As a result,  we are not using (but
; wating) the first 256 bytes of out 2kB video RAM.
;
; (c) 2024 by Holger Zahnleiter, all rights reserved
;==============================================================================

	.cr 6502
	.tf crtc_print.hex, int

;------------------------------------------------------------------------------
; Definitions
;------------------------------------------------------------------------------

; Addresses of R6501 hardware control registers
PORT_C = 0x0002	; I/O Port C
IER    = 0x0012	; Interrupt Enable Register
MCR    = 0x0014	; Mode Control Register

; CRTC registers and video memory
SHARED_MEMORY                 = 0x0000			; IDT7132, 2k×8
VRAM_START                    = SHARED_MEMORY+0x0100
CRTC                          = 0x1000			; HD6445 CRTC-II
CRTC_ADDR_REGISTER            = CRTC+0
CRTC_DATA_REGISTER            = CRTC+1
CRTC_REG_HOR_TOTAL_CHARS      = 0
CRTC_REG_HOR_DISP_CHARS       = 1
CRTC_REG_HOR_SYNC_POS         = 2
CRTC_REG_SYNC_WIDTH           = 3
CRTC_REG_VERT_TOTAL_CHARS     = 4
CRTC_REG_VERT_TOTAL_ADJUST    = 5
CRTC_REG_VERT_DISP_CHARS      = 6
CRTC_REG_VERT_SYNC_POS        = 7
CRTC_REG_INTERLACE_AND_SKEW   = 8
CRTC_REG_MAX_RASTER_ADDR      = 9
CRTC_REG_CURSOR_1_START       = 10
CRTC_REG_CURSOR_1_END         = 11
CRTC_REG_SCRN_1_START_ADDR_HI = 12
CRTC_REG_SCRN_1_START_ADDR_LO = 13
CRTC_REG_CURSOR_1_ADDR_HI     = 14
CRTC_REG_CURSOR_1_ADDR_LO     = 15
CRTC_REG_SMOOTH_SCROLLING     = 29
CRTC_REG_CONTROL_1            = 30
CRTC_REG_CONTROL_2            = 31
CRTC_REG_CONTROL_3            = 32
CRTC_REG_MEM_WIDTH_OFFSET     = 33

; 	.sm  ram
; 	.org 0x0040

; PTR:
; 	.dw 0


;------------------------------------------------------------------------------
; Program
;------------------------------------------------------------------------------

	.sm  code
	.org 0xE000

MAIN:
	sei			; Disable all interrupts globally

	cld			; Use binary instead of BCD arithmetic

	ldx #0xff		; Stackpointer points to 0xFF and grows towards
	txs			; 0x00.

	lda #0b00000000		; Disable all interrupts individually
	sta IER
	
	lda #0b01000000		; Select "normal" bus mode
	sta MCR

	jsr INIT_CRTC
	jsr FILL_FRAME_BUF

	ldy #0
.PRINT_LOOP
	; jsr READ_CRTC_STATUS	; Check either...
	; cmp #0b00000010	; ...Vertical Retrace or...
	; bne .PRINT_LOOP
	; cmp #0b10000000	; ...Memory Inhibit
	; beq .PRINT_LOOP

	lda .HELLO,y
	sta VRAM_START,y
	iny
	cpy #25
	bne .PRINT_LOOP

.LOOP
	lda #0b11111111		; Keep all port pins high for some time
	sta PORT_C
	jsr DELAY

	lda #0b00000000		; Keep all port pins low for some time
	sta PORT_C
	jsr DELAY

	jmp .LOOP		; Repeat the above to make port pins toggle
				; periodically
.HELLO
	.db "****  Hello, world!  ****"


;------------------------------------------------------------------------------
; Subroutines
;------------------------------------------------------------------------------

INIT_CRTC:
	ldx #112			; Total columns (screen width)
	ldy #CRTC_REG_HOR_TOTAL_CHARS
	jsr SET_CRTC_REGISTER
	ldx #80				; Visible columns
	ldy #CRTC_REG_HOR_DISP_CHARS
	jsr SET_CRTC_REGISTER
	ldx #86
	ldy #CRTC_REG_HOR_SYNC_POS
	jsr SET_CRTC_REGISTER

	ldx #0b11111111
	ldy #CRTC_REG_SYNC_WIDTH
	jsr SET_CRTC_REGISTER
	ldx #15				; Total rows (screen height)
	ldy #CRTC_REG_VERT_TOTAL_CHARS
	jsr SET_CRTC_REGISTER
	ldx #4
	ldy #CRTC_REG_VERT_TOTAL_ADJUST
	jsr SET_CRTC_REGISTER
	ldx #15				; Visible rows
	ldy #CRTC_REG_VERT_DISP_CHARS
	jsr SET_CRTC_REGISTER
	ldx #15
	ldy #CRTC_REG_VERT_SYNC_POS
	jsr SET_CRTC_REGISTER
	ldx #0b01010000
	ldy #CRTC_REG_INTERLACE_AND_SKEW
	jsr SET_CRTC_REGISTER
	ldx #15
	ldy #CRTC_REG_MAX_RASTER_ADDR
	jsr SET_CRTC_REGISTER
	ldx #0b01100000
	ldy #CRTC_REG_CURSOR_1_START
	jsr SET_CRTC_REGISTER
	ldx #16
	ldy #CRTC_REG_CURSOR_1_END
	jsr SET_CRTC_REGISTER
	ldx #VRAM_START>>8		; High byte of 16 bit word
	ldy #CRTC_REG_SCRN_1_START_ADDR_HI
	jsr SET_CRTC_REGISTER
	ldx #VRAM_START&0x00FF		; Low byte of 16 bit word
	ldy #CRTC_REG_SCRN_1_START_ADDR_LO
	jsr SET_CRTC_REGISTER
	ldx #VRAM_START>>8
	ldy #CRTC_REG_CURSOR_1_ADDR_HI
	jsr SET_CRTC_REGISTER
	ldx #VRAM_START&0x00FF
	ldy #CRTC_REG_CURSOR_1_ADDR_LO
	jsr SET_CRTC_REGISTER
	ldx #0
	ldy #CRTC_REG_SMOOTH_SCROLLING
	jsr SET_CRTC_REGISTER
	ldx #0b00101000
	ldy #CRTC_REG_CONTROL_1
	jsr SET_CRTC_REGISTER
	ldx #0b00000000
	ldy #CRTC_REG_CONTROL_2
	jsr SET_CRTC_REGISTER
	ldx #0b01000000
	ldy #CRTC_REG_CONTROL_3
	jsr SET_CRTC_REGISTER
	ldx #0
	ldy #CRTC_REG_MEM_WIDTH_OFFSET
	jsr SET_CRTC_REGISTER
	rts

SET_CRTC_REGISTER:
	sty CRTC_ADDR_REGISTER		; Select CRTC register y
	stx CRTC_DATA_REGISTER		; Write value x to CRTC register y
	rts


READ_CRTC_STATUS:
	lda #CRTC_REG_CONTROL_2
	sta CRTC_ADDR_REGISTER		; Select CRTC status register
	nop
	lda CRTC_DATA_REGISTER		; Status now in MCU register A
	rts


FILL_FRAME_BUF:
	ldx #0
	lda #'-'
.LOOP
	sta VRAM_START,x
	inx
	bne .LOOP
	rts


DELAY:
	ldx #0xff
        ldy #0xff
.LOOP
        dex
        bne .LOOP
        dey
        bne .LOOP
	rts
		
;------------------------------------------------------------------------------
; Interrupt service routines
;------------------------------------------------------------------------------

NMI_ISR:
	rti

IRQ_BRK_ISR:
	rti

;------------------------------------------------------------------------------
; Hardware vectors
;------------------------------------------------------------------------------
		
	.org 0xfffa
	.dw NMI_ISR	; NMI vector
	.dw MAIN	; RESET vector
	.dw IRQ_BRK_ISR	; IRQ and BRK vector

	.end
