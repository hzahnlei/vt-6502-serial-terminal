;==============================================================================
; Receives bytes from the serial interface using interrupts. The bytes are then
; buffered in a  ring buffer.  The sender side of this program  reads from that
; buffer and echoes back to the host computer via serial interface.  (This time
; not using interrupts.)
;
; (c) 2024 by Holger Zahnleiter, all rights reserved
;==============================================================================

	.cr 6502
	.tf rcv_buf.hex, int

;------------------------------------------------------------------------------
; Definitions
;------------------------------------------------------------------------------

; Addresses of R6501 hardware control registers
PORT_C         = 0x0002		; I/O Port C

IFR            = 0x0011		; Interrupt Flag Register
IFR__RCVR_FLAG = 0b01000000	; 1, if byte received via serial interface.
				; Cleared when SCSR0-3 are cleared, which is
				; the case SCDAT is read

IER            = 0x0012		; Interrupt Enable Register
IER__RCVR_FLAG = IFR__RCVR_FLAG	; Enable interrupt on serial receive 

MCR            = 0x0014		; Mode Control Register

SCCR           = 0x0015		; Serial Communication Control Register
SCSR           = 0x0016		; Serial Communication Status Register
SCSR__RDRF     = 0b00000001	; SCSR flag Receiver Data Register Full
SCSR__TDRE     = 0b01000000  	; SCSR flag Transmitter Data Register Empty
SCDAT          = 0x0017		; Serial Communication Data Register. Reading
				; this register will clear the receiver inter-
				; rupt flag

LLA            = 0x0018		; Lower Latch counter A
ULA            = 0x0019		; Upper Latch counter A

;------------------------------------------------------------------------------
; Memory variables
; Define variables upfront so that SBASM can emit code that uses zero page ad-
; dressing (faszer) over absolute addressing (slower).
;------------------------------------------------------------------------------

	.sm  ram
	.org 0X0040

INPUT_BUF:
	.db 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
INPUT_BUF_HEAD:
	.db 0
INPUT_BUF_TAIL:
	.db 0
INPUT_BUF_SIZE = INPUT_BUF_HEAD-INPUT_BUF

INPUT_BUFFER_PUT_STATUS:
	.db INPUT_BUFFER_PUT_STATUS__OK
INPUT_BUFFER_PUT_STATUS__OK       = 0
INPUT_BUFFER_PUT_STATUS__OVERFLOW = 1


;------------------------------------------------------------------------------
; Program
;------------------------------------------------------------------------------

	.sm  code
	.org 0xE000

MAIN:
	sei			; Disable all interrupts globally

	ldx #0xff		; Stackpointer points to 0xFF and grows towards
	txs			; 0x00.

	cld			; Use binary instead of BCD arithmetic

	lda #0b00000000		; Disable all interrupts individually
	ora #IER__RCVR_FLAG	; Enable interrupt in case byte is received
	sta IER			; via serial interface
	
	lda #0b01000000		; Select "normal" bus mode, both timers in
	sta MCR			; interval mode

	lda #0b11000000		; Enable XMTR/RCVR, async, 8N1(actually it is
	sta SCCR		; 8N2 but communication partners may use 8N1)

	lda #0x00		; Counter A (frequency divider) latch (re-load 
	sta ULA			; value) set to 0x000B for standard Baud rate
	lda #0x0B		; of 9.600 bps (assuming MCU clocked at
	sta LLA			; 1.8432MHz)

	lda #0			; Input buffer pointer for write and read are
	sta INPUT_BUF_HEAD	; both pointing to the beginning of the buffer
	sta INPUT_BUF_TAIL

	cli			; Allow interrupts globally now that system is
				; initialized
.LOOP
	lda #0b11111111		; Keep all port pins high for some time
	sta PORT_C
	jsr DELAY

	lda #0b00000000		; Keep all port pins low for some time
	sta PORT_C
	jsr DELAY

.ECHO_BYTE_IF_AVAILABLE
	jsr INPUT_BUFFER_GET	; Try take byte from receive ring buffer
	cpx #INPUT_BUFFER_GET__FOUND
	bne .NOTHING_RECEIVED
	tay			; If byte available, then echo back
	jsr SERIAL_SEND_BYTE
	jmp .ECHO_BYTE_IF_AVAILABLE ; Deplete buffer

.NOTHING_RECEIVED
	lda INPUT_BUFFER_PUT_STATUS	; Finally check if there were
	cmp #INPUT_BUFFER_PUT_STATUS__OVERFLOW	; input buffer overflows
.HALT
	beq .HALT	; Instead of halting we coud beep and reset the
			; buffer
	jmp .LOOP		; Repeat the above to make port pins toggle
				; periodically

;------------------------------------------------------------------------------
; Subroutines
;------------------------------------------------------------------------------

;---- Bussy waiting delay ----
DELAY:
        ldx #0xff
        ldy #0xff
.LOOP
        dex
        bne .LOOP
        dey
        bne .LOOP
        rts


;---- Put one byte, received via serial interface, into ring buffer ----
; This function is to be called from within  the interupt service routine (ISR)
; of the serial receiver. Interrupt are inhibited already, therefore no inhibi-
; ting of interrupts to protect buffer access required here.
;
; The current byte, received from the serial interface, is expected in register
; A.
; After this routine  has endend,  INPUT_BUFFER_PUT_STATUS  will signal whether
; the byte was aktually buffered,  or a buffer overflow has hindered storage of
; the byte.
INPUT_BUFFER_PUT:
	ldy INPUT_BUF_HEAD	; Remember current head
	ldx INPUT_BUF_HEAD	; Advance head by one entry
	inx
	cpx #INPUT_BUF_SIZE	; Check if head out of bounds
	bcc .CHECK_OVERFLOW
.WRAP_HEAD_AROUND
	ldx #0			; Head within bounds again
.CHECK_OVERFLOW
	cpx INPUT_BUF_TAIL	; If head == tail, then error
	beq .BUFFER_OVERFLOW
	stx INPUT_BUF_HEAD	; Remember new head and consume byte from
	sta INPUT_BUF,y		; buffer at current head
	lda #INPUT_BUFFER_PUT_STATUS__OK
	sta INPUT_BUFFER_PUT_STATUS
	rts
.BUFFER_OVERFLOW
	lda #INPUT_BUFFER_PUT_STATUS__OVERFLOW
	sta INPUT_BUFFER_PUT_STATUS
	rts


;---- Get one byte, received via serial interface, from ring buffer ----
; Byte taken from the buffer will be available in register A.
; Register X will tell whether the buffer contained any data. Register A
; is valid only if register X=1.
INPUT_BUFFER_GET:
	sei			; Prevent interrupts, only one side (reader or
				; writer) must access the buffer at any time

	ldx INPUT_BUF_TAIL	; Check if the buffer contains any data
	cpx INPUT_BUF_HEAD	; head == tail, then buffer empty
	beq .BUFFER_EMPTY
.BUFFER_NOT_EMPTY
	lda INPUT_BUF,x		; Take data from buffer

	inx			; Advance tail by one entry
	cpx #INPUT_BUF_SIZE	; Check if tail out of bounds
	bcc .RETURN_BYTE
.WRAP_TAIL_AROUND
	ldx #0			; Tail within bounds again
.RETURN_BYTE
	stx INPUT_BUF_TAIL
	ldx #INPUT_BUFFER_GET__FOUND
	jmp .DONE
.BUFFER_EMPTY
	ldx #INPUT_BUFFER_GET__EMPTY
.DONE
	cli			; Allow interrupts again
	rts

INPUT_BUFFER_GET__FOUND = 1
INPUT_BUFFER_GET__EMPTY = 0


;---- Send byte via serial interface ----
; Byte to be sent expected in register Y.
SERIAL_SEND_BYTE:
	lda SCSR		; Check if hardware transmit buffer empty/ready
	and #SCSR__TDRE 	
	beq SERIAL_SEND_BYTE
	sty SCDAT		; Hardware transmit buffer ready -> send byte
	rts


;------------------------------------------------------------------------------
; Interrupt service routines
;------------------------------------------------------------------------------

;---- Non maskable interrupt, not used ----
NMI_ISR:
	rti


;---- Interrupt request and BRK ----
IRQ_BRK_ISR:
	sei			; Suppress all interrupts
	pha			; Save register A
	tya			; Save register Y
	pha
	
	lda IER			; Check which interrupt source (out of 8
	and #IER__RCVR_FLAG	; possible sources)
	beq .DONE
.HANDLE_SERIAL_RECEIVE
	lda SCDAT		; Read byte received (also clears SCSR__RDRF).
	jsr INPUT_BUFFER_PUT	; Main routine should check
				; INPUT_BUFFER_PUT_STATUS
.DONE
	pla			; Restore register Y
	tay
	pla			; Restore register A
	cli			; Permit all interrupts
	rti

;------------------------------------------------------------------------------
; Hardware vectors
;------------------------------------------------------------------------------
		
	.org 0xfffa
	.dw NMI_ISR	; NMI vector
	.dw MAIN	; RESET vector
	.dw IRQ_BRK_ISR	; IRQ and BRK vector

	.end
