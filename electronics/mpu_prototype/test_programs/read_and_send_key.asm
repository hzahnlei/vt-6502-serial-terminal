;==============================================================================
; Reads regular keys  from keyboard matrix (no modifier keys)  and sends key to
; host computer via serial interface.
;
; (c) 2024 by Holger Zahnleiter, all rights reserved
;==============================================================================

	.cr 6502
	.tf read_and_send_key.hex, int

;------------------------------------------------------------------------------
; Definitions
;------------------------------------------------------------------------------

; Addresses of R6501 hardware control registers
PORT_B    = 0x0001	; I/O Port C
PORT_C    = 0x0002	; I/O Port C
PORT_D    = 0x0003	; I/O Port D

IER       = 0x0012	; Interrupt Enable Register

MCR       = 0x0014	; Mode Control Register


SCCR      = 0x0015	; Serial Communication Control Register
SCSR      = 0x0016	; Serial Communication Status Register
SCSR_RDRF = 0b00000001	; SCSR flag Receiver Data Register Full
SCSR_TDRE = 0b01000000  ; SCSR flag Transmitter Data Register Empty
SCDAT     = 0x0017	; Serial Communication Data Register

LLA       = 0x0018	; Lower Latch counter A
ULA       = 0x0019	; Upper Latch counter A 

;------------------------------------------------------------------------------
; Program
;------------------------------------------------------------------------------

	.org 0xE000
MAIN:
	sei			; Disable all interrupts globally

	cld			; Use binary instead of BCD arithmetic

	ldx #0xff		; Stackpointer points to 0xFF and grows towards
	txs			; 0x00.

	lda #0b00000000		; Disable all interrupts individually
	sta IER
	
	lda #0b01100000		; Select "normal" bus mode, port D all outputs,
	sta MCR			; both timers in interval mode

	lda #0b11000000		; Enable XMTR/RCVR, async, 8N1 (actually it is
	sta SCCR		; 8N2 but communication partners may use 8N1)

	lda #0x00		; Counter A (frequency divider) latch (re-load 
	sta ULA			; value) set to 0x000B for standard Baud rate
	lda #0x0B		; of 9.600 bps (assuming MCU clocked at
	sta LLA			; 1.8432MHz)

	; This did not work when setting all pins to logic low (0)! The behavi-
	; our of  the R6501's I/O ports  deviates from  what I am used to  from
	; 6522  and similar I/O chips.  The R6501 does not know  data direction
	; registers.
	lda #0b11111111		; Port B all inputs that need to be pulled down
	sta PORT_B
.LOOP
	jsr READ_AND_SEND_KEY
	; ldx #0b00101100
	; stx PORT_D

	lda #0b11111111		; Keep all port pins high for some time
	sta PORT_C
	jsr DELAY
	lda #0b00000000		; Keep all port pins low for some time
	sta PORT_C
	jsr DELAY

	jmp .LOOP		; Repeat the above to query keyboard matrix
				; periodically

;------------------------------------------------------------------------------
; Subroutines
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
READ_AND_SEND_KEY:
	;           ROW
	ldx #0b00000000		; Set KbRowX and KbColY, start with (0,0)
	;        COL
.READ
	stx PORT_D		; Write row/col to output
	
	lda PORT_B		; Read key
	and #0b00000001		; Send code for key pressed
	bne .NO_KEY_PRESSED

.KEY_PRESSED
	jmp .SEND_CHAR		; Scan code in X


.NO_KEY_PRESSED
	inx
	cpx #64			;aka 0x40 (max 64 keys)
	bne .READ
	rts  

.SEND_CHAR
	ldy .SCAN_CODE,x	; Determine scan code and send scan code, but
	cpy #0			; only if scan code is valid
	bne .SEND_WHEN_READY
	rts
.SEND_WHEN_READY
	lda SCSR		; Loop for as long transmit buffer not ready
	and #SCSR_TDRE 	
	beq .SEND_CHAR	
	sty SCDAT		; Buffer is ready, therefore send char in Y
				; register
	rts
.SCAN_CODE:
	; I cannot allign the mapped codes with the comments as SBASM will give
	; me error messages then. Additional white space seems to confuse SBASM
	;;; 0x00      | 0x01 INSRT | 0x02     | 0x03 ATARI | 0x04     |0x05 DWN | 0x06    | 0x07 RGHT
	.db 'A', '>', 'Q', 0x00, '1', '=', 'Z', '*'
	;;; 0x08      | 0x09 CLR   | 0x0A     | 0x0B       | 0x0C     | 0x0D UP | 0x0E    | 0x0F LFT
	.db 'S', '<', 'W', '/', '2', '-', 'X', '+'
	;;; 0x10      | 0x11       | 0x12     | 0x13       | 0x14     | 0x15    | 0x16    | 0x17
	.db 'G', '8', 'T', 'M', '5', 'I', 'B', 'K'
	;;; 0x18 CAPS | 0x19 BCKSP | 0x1A TAB | 0x1B NA    | 0x1C ESC | 0x1D CR | 0x1E NA | 0x1F NA
	.db 0x00, 0x08, 0x09, 0x00, 0x1B, 0x0D, 0x00, 0x00
	;;; 0x20 NA   | 0x21       | 0x22     | 0x23       | 0x24     | 0x25    | 0x26 NA | 0x27 NA
	.db 0x00, '7', 'Y', 'N', '6', 'U', 0x00, 0x00
	;;; 0x28      | 0x29       | 0x2A     | 0x2B       | 0x2C     | 0x2D    | 0x2E    | 0x2F
	.db 'D', '0', 'E', '.', '3', 'P', 'C', ';'
	;;; 0x30      | 0x31 NA    | 0x32 NA  | 0x33 SPACE | 0x34 NA  | 0x35 NA | 0x36 NA | 0x37
	.db 'H', 0x00, 0x00, ' ', 0x00, 0x00, 0x00, 'J'
	;;; 0x38      | 0x39       | 0x3A     | 0x3B       | 0x3C     | 0x3D    | 0x3E    | 0x3F
	.db 'F', '9', 'R', ',', '4', 'O', 'V', 'L'


;------------------------------------------------------------------------------
DELAY:
	ldx #0xff
	ldy #0xff
.LOOP
        dex
        bne .LOOP
        dey
        bne .LOOP
	rts
	
;------------------------------------------------------------------------------
; Interrupt service routines
;------------------------------------------------------------------------------

NMI_ISR:
	rti

IRQ_BRK_ISR:
	rti

;------------------------------------------------------------------------------
; Hardware vectors
;------------------------------------------------------------------------------
		
	.org 0xfffa
	.dw NMI_ISR	; NMI vector
	.dw MAIN	; RESET vector
	.dw IRQ_BRK_ISR	; IRQ and BRK vector

	.end
