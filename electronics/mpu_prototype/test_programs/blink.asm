;==============================================================================
; Toggle all bits of digital output port D, so that we can see whether the cir-
; cuit is working or not.  Observing the blinking port can be achieved by means
; of an ordinary voltmeter or by attaching an LED.
; In this example we are using  port D for blinking  whereas the other examples
; use port C.
;
; (c) 2024 by Holger Zahnleiter, all rights reserved
;==============================================================================

	.cr 6502
	.tf blink.hex, int

;------------------------------------------------------------------------------
; Definitions
;------------------------------------------------------------------------------

; Addresses of R6501 hardware control registers
PORT_D = 0x0003	; I/O Port D
IER    = 0x0012	; Interrupt Enable Register
MCR    = 0x0014	; Mode Control Register

;------------------------------------------------------------------------------
; Program
;------------------------------------------------------------------------------

	.org 0xE000
MAIN:
	sei			; Disable all interrupts globally

	cld			; Use binary instead of BCD arithmetic

	ldx #0xff		; Stackpointer points to 0xFF and grows towards
	txs			; 0x00.
	
	lda #0b00000000		; Disable all interrupts individually
	sta IER

	lda #0b01100000		; Select "normal" bus mode, port D as output
	sta MCR

.LOOP
	lda #0b11111111		; Keep all port pins high for some time
	sta PORT_D
	jsr DELAY

	lda #0b00000000		; Keep all port pins low for some time
	sta PORT_D
	jsr DELAY

	jmp .LOOP		; Repeat the above to make port pins toggle
				; periodically

;------------------------------------------------------------------------------
; Subroutines
;------------------------------------------------------------------------------

DELAY:
        ldx #0xff
        ldy #0xff
.LOOP
        dex
        bne .LOOP
        dey
        bne .LOOP
        rts
	
;------------------------------------------------------------------------------
; Interrupt service routines
;------------------------------------------------------------------------------

NMI_ISR:
	rti

IRQ_BRK_ISR:
	rti

;------------------------------------------------------------------------------
; Hardware vectors
;------------------------------------------------------------------------------
		
	.org 0xfffa
	.dw NMI_ISR	; NMI vector
	.dw MAIN	; RESET vector
	.dw IRQ_BRK_ISR	; IRQ and BRK vector

	.end
