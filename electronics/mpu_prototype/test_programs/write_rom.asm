;==============================================================================
; Writing to the AT28C64 EEPROM works.  However,  the progam - residing in that
; same EEPROM - then crashes. The programming cycle takes longer, so the ROM is
; not ready when the next opcode is to be fetched.
; Therefore I am trying the following approach here:  I am copying the code for
; writing to ROM from ROM into RAM.  Therefore,  the ROM writing happens from a
; program executed from RAM.  Therefore, the next fetch  is also from RAM.  The
; programming routine will  delay briefly until the ROM  is ready before retur-
; ning control back to the program in ROM.
;
; For some reason I cannot reproduce the writing to EEPROM. The program at does
; not crash anymore but the byte im EEPROM does not change its initial value.
;
; (c) 2024 by Holger Zahnleiter, all rights reserved
;==============================================================================

	.cr 6502
	.tf write_rom.hex, int

;------------------------------------------------------------------------------
; Definitions
;------------------------------------------------------------------------------

; Addresses of R6501 hardware control registers
PORT_C = 0x0002	; I/O Port C
MCR    = 0x0014	; Mode Control Register

WRITE_TO_ROM = 0x0040	; Address of routine in RAM

;------------------------------------------------------------------------------
; Program
;------------------------------------------------------------------------------

	.org 0xE000
MAIN:
	sei			; Disable all interrupts globally

	cld			; Use binary instead of BCD arithmetic

	ldx #0xff		; Stackpointer points to 0xFF and grows towards
	txs			; 0x00.
	
	lda #0b01000000		; Select "normal" bus mode
	sta MCR

	jsr COPY_WRITE_ROUTINE_TO_RAM

	jsr WRITE_TO_ROM

.LOOP
	lda #0b11111111		; Keep all port pins high for some time
	sta PORT_C
	jsr DELAY

	lda #0b00000000		; Keep all port pins low for some time
	sta PORT_C
	jsr DELAY

	jmp .LOOP		; Repeat the above to make port pins toggle
				; periodically

;------------------------------------------------------------------------------
; Subroutines
;------------------------------------------------------------------------------

COPY_WRITE_ROUTINE_TO_RAM:
	ldx #0
.LOOP
	lda _WRITE_TO_ROM_,x
	sta WRITE_TO_ROM,x
	inx
	cpx #16
	bne .LOOP
	rts

; This routine will be copied to RAM, hence it will be executed from RAM.  As a
; result the write cycle time of  the EEPROM should not crash the program as no
; fetch from EEPROM occurs while it is still busy with writing.
_WRITE_TO_ROM_:
	lda #0xA5
	sta ROM_VAR
	ldx #0xff
	ldy #0xff
.LOOP
        dex
        bne .LOOP
        dey
        bne .LOOP
        rts

DELAY:
	ldx #0xff
	ldy #0xff
.LOOP
        dex
        bne .LOOP
        dey
        bne .LOOP
        rts
	
;------------------------------------------------------------------------------
; Interrupt service routines
;------------------------------------------------------------------------------

NMI_ISR:
	rti

IRQ_BRK_ISR:
	rti

	.org 0xfff0
ROM_VAR:
	.db 0x00		; Should read 0xA5 after execution

;------------------------------------------------------------------------------
; Hardware vectors
;------------------------------------------------------------------------------
		
	.org 0xfffa
	.dw NMI_ISR	; NMI vector
	.dw MAIN	; RESET vector
	.dw IRQ_BRK_ISR	; IRQ and BRK vector

	.end
