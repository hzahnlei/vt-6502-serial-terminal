;==============================================================================
; Just run a tight loop.  As a result the  address lines are relatively static.
; They should have this value: 0xE00? = 0b11100000000000??. Because of the sta-
; tic behaviour of the address lines in this code example the address lines can
; be easily observed/inspected unsing a voltmeter.  No complex equipment requi-
; red.  Change of address line A0  can be made visible with a  simple frequency
; divider.
;
; (c) 2024 by Holger Zahnleiter, all rights reserved
;==============================================================================

	.cr 6502
	.tf most_simple.hex, int

;------------------------------------------------------------------------------
; Definitions
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
; Program
;------------------------------------------------------------------------------

	.org 0xE000
MAIN:
	jmp MAIN

;------------------------------------------------------------------------------
; Subroutines
;------------------------------------------------------------------------------
	
;------------------------------------------------------------------------------
; Interrupt service routines
;------------------------------------------------------------------------------

NMI_ISR:
	rti

IRQ_BRK_ISR:
	rti

;------------------------------------------------------------------------------
; Hardware vectors
;------------------------------------------------------------------------------
		
	.org 0xfffa
	.dw NMI_ISR	; NMI vector
	.dw MAIN	; RESET vector
	.dw IRQ_BRK_ISR	; IRQ and BRK vector

	.end
