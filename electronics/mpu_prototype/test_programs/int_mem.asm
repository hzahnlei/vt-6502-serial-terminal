;==============================================================================
; Toggle all bits of digital output port C, so that we can see whether the cir-
; cuit is working or not.  Observing the blinking port can be achieved by means
; of an ordinary voltmeter or by attaching an LED to PC0.
; This variant of a "blinking" program  stores the loop counter  in the R6501's
; internal RAM. So this is an internal memory test.
;
; (c) 2024 by Holger Zahnleiter, all rights reserved
;==============================================================================

	.cr 6502
	.tf int_mem.hex, int

;------------------------------------------------------------------------------
; Definitions
;------------------------------------------------------------------------------

; Addresses of R6501 hardware control registers
PORT_C = 0x0002	; I/O Port C
MCR    = 0x0014	; Mode Control Register

;------------------------------------------------------------------------------
; Program
;------------------------------------------------------------------------------

	.org 0xE000
MAIN:
	sei			; Disable all interrupts globally

	cld			; Use binary instead of BCD arithmetic

	ldx #0xff		; Stackpointer points to 0xFF and grows towards
	txs			; 0x00.
	
	lda #0b01000000		; Select "normal" bus mode
	sta MCR

.LOOP
	lda #0b11111111		; Keep all port pins high for some time
	sta PORT_C
	jsr DELAY

	lda #0b00000000		; Keep all port pins low for some time
	sta PORT_C
	jsr DELAY

	jmp .LOOP		; Repeat the above to make port pins toggle
				; periodically

;------------------------------------------------------------------------------
; Subroutines
;------------------------------------------------------------------------------

DELAY:
	lda #0xff
	sta 0x40
	sta 0x41
.LOOP
        dec 0x40
        bne .LOOP
        dec 0x41
        bne .LOOP
        rts
	
;------------------------------------------------------------------------------
; Interrupt service routines
;------------------------------------------------------------------------------

NMI_ISR:
	rti

IRQ_BRK_ISR:
	rti

;------------------------------------------------------------------------------
; Hardware vectors
;------------------------------------------------------------------------------
		
	.org 0xfffa
	.dw NMI_ISR	; NMI vector
	.dw MAIN	; RESET vector
	.dw IRQ_BRK_ISR	; IRQ and BRK vector

	.end
