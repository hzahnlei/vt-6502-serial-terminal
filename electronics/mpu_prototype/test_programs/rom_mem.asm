;==============================================================================
; Toggle all bits of digital output port C, so that we can see whether the cir-
; cuit is working or not.  Observing the blinking port can be achieved by means
; of an ordinary voltmeter or by attaching an LED to PC0.
; This variant of a "blinking" program  stores a value in the ROM chip. The ROM
; chip is actually an  EEPROM of type AT28C64.  The terminal software may store
; configuration parameters in ROM (baud rate, number of stop bits, etc.)
; When programming  this program  into the EEPROM memory location  ROM_VAR gets
; set to a value 0f 0x00.  After program execution one may read out the EEPROM.
; Memory address ROM_VAR should now read 0xA5.
;
; Be advised that writing to ROM works.  But,  the program than seems to crash.
; It does not matter  whether the write-to address is in the  same 64 byte page
; of the program or outside.  Maybe the AT28C64's  RDY/BUSY line should be used
; to halt the MCU until the  write cycle has completed.  This also crashes when
; using a faster and paged AT28C256.
;
; For some reason I cannot reproduce the writing to EEPROM. The program crashes
; but the byte im EEPROM does not change its initial value.
;
; (c) 2024 by Holger Zahnleiter, all rights reserved
;==============================================================================

	.cr 6502
	.tf rom_mem.hex, int

;------------------------------------------------------------------------------
; Definitions
;------------------------------------------------------------------------------

; Addresses of R6501 hardware control registers
PORT_C = 0x0002	; I/O Port C
MCR    = 0x0014	; Mode Control Register

;------------------------------------------------------------------------------
; Program
;------------------------------------------------------------------------------

	.org 0xE000
MAIN:
	sei			; Disable all interrupts globally

	cld			; Use binary instead of BCD arithmetic

	ldx #0xff		; Stackpointer points to 0xFF and grows towards
	txs			; 0x00.
	
	lda #0b01000000		; Select "normal" bus mode
	sta MCR

	lda #0xA5		; The memory location at ROM_VAR should read
	sta .ROM_VAR		; 0xA5 after execution
	
	; Crashes after the write operation. Probably AT28C64 cannot provide
	; next op-code because write cycle not completed. R6501 requesting next
	; op-code faster than AT28C64 write clycle

	; Code below here not executed

.LOOP
	lda #0b11111111		; Keep all port pins high for some time
	sta PORT_C
	jsr DELAY

	lda #0b00000000		; Keep all port pins low for some time
	sta PORT_C
	jsr DELAY

	jmp .LOOP		; Repeat the above to make port pins toggle
				; periodically
.ROM_VAR
	.db 0x00		; Should read 0xA5 after execution

;------------------------------------------------------------------------------
; Subroutines
;------------------------------------------------------------------------------

DELAY:
	ldx #0xff
	ldy #0xff
.LOOP
        dex
        bne .LOOP
        dey
        bne .LOOP
        rts
	
;------------------------------------------------------------------------------
; Interrupt service routines
;------------------------------------------------------------------------------

NMI_ISR:
	rti

IRQ_BRK_ISR:
	rti

;------------------------------------------------------------------------------
; Hardware vectors
;------------------------------------------------------------------------------
		
	.org 0xfffa
	.dw NMI_ISR	; NMI vector
	.dw MAIN	; RESET vector
	.dw IRQ_BRK_ISR	; IRQ and BRK vector

	.end
