/*
        I am using an older Arduino Nano clone with CH340G USB serial chip.
        For heavens sake the USB serial driver is included with more recent versions of macOS nowerdays.
        Uploding this sketch only works when picking "Arduino Nano" and "ATmega328P (Old Bootloader)".
        Of cause one needs to pick the right USB serial port too.

        Holger Zahnleiter, 2023-11-30
*/

#include "test_dongle.hpp"

/*
        The pin assignmens are reflecting the wiring of the Arduino Nano to the pins of
        address latch (74HC595), video RAM (IDT7132) and CRTC (HD6445) chips.
*/
static constexpr auto ADDR_LATCH_PIN{A0};
static constexpr auto ADDR_SER_CLK_PIN{A1};
static constexpr auto ADDR_SER_DATA_PIN{A2};

static constexpr auto MCU_RW_PIN{A4}; // 6502 R/W and 8080 WR share same Arduino pin

static constexpr auto VRAM_CE_PIN{A3};
static constexpr auto VRAM_RW_PIN{MCU_RW_PIN};
static constexpr auto VRAM_OE_PIN{A5};

static constexpr auto CRTC_CS_PIN{D3};
static constexpr auto CRTC_RD_PIN{D4};
static constexpr auto CRTC_WR_PIN{MCU_RW_PIN};

static constexpr auto DATA_PIN_0{D5};
static constexpr auto DATA_PIN_1{D6};
static constexpr auto DATA_PIN_2{D7};
static constexpr auto DATA_PIN_3{D8};
static constexpr auto DATA_PIN_4{D9};
static constexpr auto DATA_PIN_5{D10};
static constexpr auto DATA_PIN_6{D11};
static constexpr auto DATA_PIN_7{D12};

static constexpr auto FONT_LSB_PIN{D2};
static constexpr auto FONT_MSB_PIN{D13};

static constexpr Bus_Pins DATA_BUS_PINS{
        DATA_PIN_0, DATA_PIN_1, DATA_PIN_2, DATA_PIN_3,
        DATA_PIN_4, DATA_PIN_5, DATA_PIN_6, DATA_PIN_7};

Address_Bus addr_bus{ADDR_LATCH_PIN, ADDR_SER_CLK_PIN, ADDR_SER_DATA_PIN};
Data_Bus data_bus{DATA_BUS_PINS};
IDT7132 video_ram{addr_bus, data_bus, VRAM_CE_PIN, VRAM_RW_PIN, VRAM_OE_PIN};
HD6445 crtc{addr_bus, data_bus, CRTC_CS_PIN, CRTC_RD_PIN, CRTC_WR_PIN};
Font_Selector font{FONT_LSB_PIN, FONT_MSB_PIN};
Test_Dongle test_dongle{video_ram, crtc, font};

auto print_intro() -> void; // Forward declaration

auto setup() -> void {
        Serial.begin(9600);
        while (!Serial) {
        }
        print_intro();

        // A quick CRTC register read/write test. Only activate when not connected to the tube (CRT).
        // Serial.println();
        // test_dongle.test_crtc_registers();
        
        Serial.println();
        //test_dongle.configure_wp1_crt();
        test_dongle.configure_80_by_15_crt();
        Serial.println();
        test_dongle.test_video_memory(2 * 1024);
        Serial.println();
        test_dongle.populate_video_memory_cp850(2 * 1024);
        //test_dongle.set_video_memory(2 * 1024, ' ');
        Serial.println();
        test_dongle.display_some_text();
        Serial.println();
        test_dongle.cycle_fonts_forever();
        Serial.println(F("Done!"));
}

auto loop() -> void {
}

auto give_serial_monitor_a_chance_to_connect() -> void; // Forward declaration

auto print_intro() -> void {
        give_serial_monitor_a_chance_to_connect();
        Serial.println(F("***********************************************************"));
        Serial.println(F("**  VT-6502 Test Dongle, V1.0.0                          **"));
        Serial.println(F("**  (c) 2023 by Holger Zahnleiter. All rights reserved.  **"));
        Serial.println(F("***********************************************************"));
}

auto give_serial_monitor_a_chance_to_connect() -> void {
        for (auto i{0U}; i < 10; i++) {
                Serial.print('.');
                delay(500);
        }
        Serial.println();
        Serial.println();
}
