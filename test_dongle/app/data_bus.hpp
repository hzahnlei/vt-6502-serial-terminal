#pragma once

#include "fundamental_definitions.hpp"

constexpr auto BUS_PIN_COUNT{8};

using Bus_Pins = int[BUS_PIN_COUNT];

class Data_Bus final {
        private:
                Bus_Pins m_pins;
                bool m_is_input;
        public:
                Data_Bus(const Bus_Pins pins) {
                        for (auto i{0U}; i < BUS_PIN_COUNT; i++) {
                                m_pins[i] = pins[i];
                        }
                        m_is_input = false;
                        enable_reading();
                }

                auto read() -> Byte {
                        clear_port_pins_for_reading();
                        enable_reading();
                        Byte value{0};
                        for (auto i{BUS_PIN_COUNT - 1}; i >= 0; i--) {
                                value *= 2;
                                value += (digitalRead(m_pins[i]) == HIGH ? 1 : 0);
                        }
                        return value;
                }

                auto write(Byte value) -> void{
                        enable_writing();
                        for (auto i{0U}; i < BUS_PIN_COUNT; i++) {
                                const auto rem{value % 2};
                                value = (value - rem) / 2;
                                digitalWrite(m_pins[i], rem == 0 ? LOW : HIGH);
                        }
                }

        private:
                inline auto clear_port_pins_for_reading() -> void {
                        for (auto i{BUS_PIN_COUNT - 1}; i >= 0; i--) {
                                digitalWrite(m_pins[i], LOW);
                        }
                }

                auto enable_reading() -> void {
                        if (!m_is_input) {
                                for (auto i{0U}; i < BUS_PIN_COUNT; i++) {
                                        pinMode(m_pins[i], INPUT);
                                }
                                m_is_input = true;
                                // delayMicroseconds(1);
                        }

                }

                auto enable_writing() -> void{
                        if (m_is_input) {
                                for (auto i{0U}; i < BUS_PIN_COUNT; i++) {
                                        pinMode(m_pins[i], OUTPUT);
                                }
                                m_is_input = false;
                                // delayMicroseconds(1);
                        }
                }
};
