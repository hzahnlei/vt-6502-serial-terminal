#pragma once

#include "address_bus.hpp"
#include "data_bus.hpp"
#include "idt7132.hpp"
#include "hd6445.hpp"
#include "font_selector.hpp"

class Test_Dongle final {
        private:
                IDT7132 &m_video_ram;
                HD6445 &m_crtc;
                Font_Selector &m_font;

        public:
                Test_Dongle(IDT7132 &video_ram, HD6445 &crtc, Font_Selector &font) : m_video_ram{video_ram}, m_crtc{crtc},
                        m_font{font} {}

                auto configure_wp1_crt() -> void {
                        Serial.print(F("Configuring HD6445 CRTC-II for 91x15 (WP-1)..."));
                        m_crtc.write(HD6445::Register::HORIZONTAL_TOTAL_CHARACTERS_WO,     117);
                        m_crtc.write(HD6445::Register::HORIZONTAL_DISPLAYED_CHARACTERS_WO,  91);
                        m_crtc.write(HD6445::Register::HORIZONTAL_SYNC_POSITION_WO,         93);
                        m_crtc.write(HD6445::Register::SYNC_WIDTH_WO,               0b11111111);
                        m_crtc.write(HD6445::Register::VERTICAL_TOTAL_ROWS_WO,              15);
                        m_crtc.write(HD6445::Register::VERTICAL_TOTAL_ADJUST_WO,             4);
                        m_crtc.write(HD6445::Register::VERTICAL_DISPLAYED_ROWS_WO,          15);
                        m_crtc.write(HD6445::Register::VERTICAL_SYNC_POSITION_WO,           15);
                        m_crtc.write(HD6445::Register::INTERLACE_MODE_AND_SKEW_WO,  0b01010000);
                        m_crtc.write(HD6445::Register::MAX_RASTER_ADDRESS_WO,               15);
                        m_crtc.write(HD6445::Register::CURSOR_1_START_WO,      0b01100000 + 16);
                        m_crtc.write(HD6445::Register::CURSOR_1_END_WO,                      0);
                        m_crtc.write(HD6445::Register::SCREEN_1_START_ADDRESS_HIGH_RW,       0); // 0x0000
                        m_crtc.write(HD6445::Register::SCREEN_1_START_ADDRESS_LOW_RW,        0);
                        m_crtc.write(HD6445::Register::CURSOR_1_ADDRESS_HIGH_RW,             0); // 0x0000
                        m_crtc.write(HD6445::Register::CURSOR_1_ADDRESS_LOW_RW,              0);
                        // Skipping LIGHT_PEN_HIGH_RO
                        // Skipping LIGHT_PEN_LOW_RO
                        m_crtc.write(HD6445::Register::SCREEN_2_START_POSITION_RW,           1);
                        m_crtc.write(HD6445::Register::SCREEN_2_START_ADDRESS_HIGH_RW,    0x04); // 0x04c0 (1216)
                        m_crtc.write(HD6445::Register::SCREEN_2_START_ADDRESS_LOW_RW,     0xc0);
                        m_crtc.write(HD6445::Register::SCREEN_3_START_POSITION_RW,           0);
                        m_crtc.write(HD6445::Register::SCREEN_3_START_ADDRESS_HIGH_RW,    0x00); // 0x00c0 ( 192)
                        m_crtc.write(HD6445::Register::SCREEN_3_START_ADDRESS_LOW_RW,     0xc0);
                        m_crtc.write(HD6445::Register::SCREEN_4_START_POSITION_RW,          12);
                        m_crtc.write(HD6445::Register::SCREEN_4_START_ADDRESS_HIGH_RW,    0x01); // 0x0180 ( 384)
                        m_crtc.write(HD6445::Register::SCREEN_4_START_ADDRESS_LOW_RW,     0x80);
                        m_crtc.write(HD6445::Register::VERTICAL_SYNC_POSITION_ADJUST_WO,     0);
                        // Skipping LIGHT_PEN_RASTER_RO
                        m_crtc.write(HD6445::Register::SMOOTH_SCROLLING_RW,                  0);
                        m_crtc.write(HD6445::Register::CONTROL_1_WO,                0b00101011);
                        m_crtc.write(HD6445::Register::CONTROL_2_WO,                0b00000000);
                        m_crtc.write(HD6445::Register::CONTROL_3_WO,                0b01001000);
                        m_crtc.write(HD6445::Register::MEMORY_WIDTH_OFFSET_RW,             101);
                        m_crtc.write(HD6445::Register::CURSOR_2_START_WO,                   32);
                        m_crtc.write(HD6445::Register::CURSOR_2_END_WO,                      0);
                        m_crtc.write(HD6445::Register::CURSOR_2_ADDRESS_HIGH_RW,             0);
                        m_crtc.write(HD6445::Register::CURSOR_2_ADDRESS_LOW_RW,              0);
                        m_crtc.write(HD6445::Register::CURSOR_1_WIDTH_RW,                    0);
                        m_crtc.write(HD6445::Register::CURSOR_2_WIDTH_RW,                    0);
                        Serial.println(F("OK"));
                }

                auto configure_80_by_15_crt() -> void {
                        // The original settings do not work with my prototype which shares with the WP-1 the 15.33MHz crystal
                        // but uses an 8x16 instead of an 9x16 font.
                        // However, these settings work with the PCB derived from the prototype (14.7456MHz, 8x16)
                        // even though the first (left most) pixel seems to be a bit thin.
//                        Serial.print(F("Configuring HD6445 CRTC-II for 91x15 characters á 9x16..."));
//                        m_crtc.write(HD6445::Register::HORIZONTAL_TOTAL_CHARACTERS_WO,     117);
//                        m_crtc.write(HD6445::Register::HORIZONTAL_DISPLAYED_CHARACTERS_WO,  91);
//                        m_crtc.write(HD6445::Register::HORIZONTAL_SYNC_POSITION_WO,         93);

                        // This optimally fills the screen when applied to the PCB version.
                        // 14.7467MHz crystal, 8x16 font
                        // However, the leftmost pixel is quite thin at this frequency.
//                        Serial.print(F("Configuring HD6445 CRTC-II for 91x15 characters á 8x16..."));
//                        m_crtc.write(HD6445::Register::HORIZONTAL_TOTAL_CHARACTERS_WO,     112);
//                        m_crtc.write(HD6445::Register::HORIZONTAL_DISPLAYED_CHARACTERS_WO,  91);
//                        m_crtc.write(HD6445::Register::HORIZONTAL_SYNC_POSITION_WO,         92);

                        // The PCB version produces a nice and stable 80x15 image. (12.88MHz, 8x16).
                        // However, the leftmost pixel starts to become quite thin at this frequency.
//                        Serial.print(F("Configuring HD6445 CRTC-II for 80x15 characters á 8x16..."));
//                        m_crtc.write(HD6445::Register::HORIZONTAL_TOTAL_CHARACTERS_WO,     96);
//                        m_crtc.write(HD6445::Register::HORIZONTAL_DISPLAYED_CHARACTERS_WO,  80);
//                        m_crtc.write(HD6445::Register::HORIZONTAL_SYNC_POSITION_WO,         81);

// The PCB version produces a nice and stable 70x15 image. (11.0592MHz, 8x16).
// However, the leftmost pixel starts to become quite thin at this frequency.
//                        Serial.print(F("Configuring HD6445 CRTC-II for 91x15 characters á 8x16..."));
//                        m_crtc.write(HD6445::Register::HORIZONTAL_TOTAL_CHARACTERS_WO,     90);
//                        m_crtc.write(HD6445::Register::HORIZONTAL_DISPLAYED_CHARACTERS_WO,  70);
//                        m_crtc.write(HD6445::Register::HORIZONTAL_SYNC_POSITION_WO,         75);

                        // These parameters were found by little experimentation.
                        // The prototype board (15.33MHz crystal, 8x16 font) produces a nice and stable 80x15 image.
                        Serial.print(F("Configuring HD6445 CRTC-II for 80x15 characters á 9x16...")); 
                        m_crtc.write(HD6445::Register::HORIZONTAL_TOTAL_CHARACTERS_WO,     104);
                        m_crtc.write(HD6445::Register::HORIZONTAL_DISPLAYED_CHARACTERS_WO,  80);
                        m_crtc.write(HD6445::Register::HORIZONTAL_SYNC_POSITION_WO,         84);

                        // The PCB version produces a nice and stable 40x15 image. (8MHz)
//                        Serial.print(F("Configuring HD6445 CRTC-II for 40x15 characters á 8x16...")); 
//                        m_crtc.write(HD6445::Register::HORIZONTAL_TOTAL_CHARACTERS_WO,     65);
//                        m_crtc.write(HD6445::Register::HORIZONTAL_DISPLAYED_CHARACTERS_WO,  40);
//                        m_crtc.write(HD6445::Register::HORIZONTAL_SYNC_POSITION_WO,         49);

                        // The PCB version produces a nice and stable 80x15 image. (8MHz)
                        // This is the maximum number of columns I could achieve with 8MHz.
//                        Serial.print(F("Configuring HD6445 CRTC-II for 80x15 characters á 8x16...")); 
//                        m_crtc.write(HD6445::Register::HORIZONTAL_TOTAL_CHARACTERS_WO,     65);
//                        m_crtc.write(HD6445::Register::HORIZONTAL_DISPLAYED_CHARACTERS_WO,  50);
//                        m_crtc.write(HD6445::Register::HORIZONTAL_SYNC_POSITION_WO,         54);


                        // Works with the PCB derived from the prototype (14.7456MHz, 8x16).
//                        Serial.print(F("Configuring HD6445 CRTC-II for 91x15 characters á 8x16..."));
//                        m_crtc.write(HD6445::Register::HORIZONTAL_TOTAL_CHARACTERS_WO,     114);
//                        m_crtc.write(HD6445::Register::HORIZONTAL_DISPLAYED_CHARACTERS_WO,  91);
//                        m_crtc.write(HD6445::Register::HORIZONTAL_SYNC_POSITION_WO,         94);

// Works with prototype
//                        Serial.print(F("Configuring HD6445 CRTC-II for 102x15 characters á 8x16..."));
//                        m_crtc.write(HD6445::Register::HORIZONTAL_TOTAL_CHARACTERS_WO,     117);
//                        m_crtc.write(HD6445::Register::HORIZONTAL_DISPLAYED_CHARACTERS_WO,  80);
//                        m_crtc.write(HD6445::Register::HORIZONTAL_SYNC_POSITION_WO,         84);

// The PCB version produces a nice and stable 28x15 image. (4.433...MHz PAL, 8x16)
//                        Serial.print(F("Configuring HD6445 CRTC-II for 28x15 characters á 8x16...")); 
//                        m_crtc.write(HD6445::Register::HORIZONTAL_TOTAL_CHARACTERS_WO,     37);
//                        m_crtc.write(HD6445::Register::HORIZONTAL_DISPLAYED_CHARACTERS_WO,  28);
//                        m_crtc.write(HD6445::Register::HORIZONTAL_SYNC_POSITION_WO,         32);

                        m_crtc.write(HD6445::Register::SYNC_WIDTH_WO,               0b11111111);
                        m_crtc.write(HD6445::Register::VERTICAL_TOTAL_ROWS_WO,              15);
                        m_crtc.write(HD6445::Register::VERTICAL_TOTAL_ADJUST_WO,             4);
                        m_crtc.write(HD6445::Register::VERTICAL_DISPLAYED_ROWS_WO,          15);
                        m_crtc.write(HD6445::Register::VERTICAL_SYNC_POSITION_WO,           15);
                        m_crtc.write(HD6445::Register::INTERLACE_MODE_AND_SKEW_WO,  0b01010000);
                        m_crtc.write(HD6445::Register::MAX_RASTER_ADDRESS_WO,               15);
                        m_crtc.write(HD6445::Register::CURSOR_1_START_WO,      0b01100000 + 0); //+16
                        m_crtc.write(HD6445::Register::CURSOR_1_END_WO,                      16); // 0
                        m_crtc.write(HD6445::Register::SCREEN_1_START_ADDRESS_HIGH_RW,       0); // 0x0000
                        m_crtc.write(HD6445::Register::SCREEN_1_START_ADDRESS_LOW_RW,        1);
                        m_crtc.write(HD6445::Register::CURSOR_1_ADDRESS_HIGH_RW,             0); // 0x0000
                        m_crtc.write(HD6445::Register::CURSOR_1_ADDRESS_LOW_RW,              1);
                        // Skipping HD6445::Register::LIGHT_PEN_HIGH_RO
                        // Skipping HD6445::Register::LIGHT_PEN_LOW_RO
                        //                        m_crtc.write(HD6445::Register::SCREEN_2_START_POSITION_RW,           1);
                        //                        m_crtc.write(HD6445::Register::SCREEN_2_START_ADDRESS_HIGH_RW,    0x04); // 0x04c0 (1216)
                        //                        m_crtc.write(HD6445::Register::SCREEN_2_START_ADDRESS_LOW_RW,     0xc0);
                        //                        m_crtc.write(HD6445::Register::SCREEN_3_START_POSITION_RW,           0);
                        //                        m_crtc.write(HD6445::Register::SCREEN_3_START_ADDRESS_HIGH_RW,    0x00); // 0x00c0 ( 192)
                        //                        m_crtc.write(HD6445::Register::SCREEN_3_START_ADDRESS_LOW_RW,     0xc0);
                        //                        m_crtc.write(HD6445::Register::SCREEN_4_START_POSITION_RW,          12);
                        //                        m_crtc.write(HD6445::Register::SCREEN_4_START_ADDRESS_HIGH_RW,    0x01); // 0x0180 ( 384)
                        //                        m_crtc.write(HD6445::Register::SCREEN_4_START_ADDRESS_LOW_RW,     0x80);
                        //                        m_crtc.write(HD6445::Register::VERTICAL_SYNC_POSITION_ADJUST_WO,     0);
                        // Skipping HD6445::Register::LIGHT_PEN_RASTER_RO
                        m_crtc.write(HD6445::Register::SMOOTH_SCROLLING_RW,                  0);
                        m_crtc.write(HD6445::Register::CONTROL_1_WO,                0b00101000);  // 0b00101011
                        m_crtc.write(HD6445::Register::CONTROL_2_WO,                0b00000000);
                        m_crtc.write(HD6445::Register::CONTROL_3_WO,                0b01000000); // 0b01001000
                        m_crtc.write(HD6445::Register::MEMORY_WIDTH_OFFSET_RW,             0); // 101
                        //                        m_crtc.write(HD6445::Register::CURSOR_2_START_WO,                   32);
                        //                        m_crtc.write(HD6445::Register::CURSOR_2_END_WO,                      0);
                        //                        m_crtc.write(HD6445::Register::CURSOR_2_ADDRESS_HIGH_RW,             0);
                        //                        m_crtc.write(HD6445::Register::CURSOR_2_ADDRESS_LOW_RW,              0);
                        //                        m_crtc.write(HD6445::Register::CURSOR_1_WIDTH_RW,                    0); //R32::CW1=0
                        //                        m_crtc.write(HD6445::Register::CURSOR_2_WIDTH_RW,                    0);
                        Serial.println(F("OK"));
                }

                auto test_video_memory(const size_t mem_size) -> void {
                        Serial.println(F("VRAM Test"));
                        write_some_bytes_and_read_back();
                        if (!mem_test(mem_size, 0x00)) {
                                return;
                        }
                        if (!mem_test(mem_size, 0xff)) {
                                return;
                        }
                        if (!mem_test(mem_size, 0xa5)) {
                                return;
                        }
                        if (!mem_test(mem_size, 0x5a)) {
                                return;
                        }
                }

                /*
                        Reading and writing arbitraty CRTC registers for testing purposes should
                        only be done while the tube is not connected. I have no idea what might
                        happen if nonsensical values are placed in the registers with the tube
                        powered.
                        I am only using this in an early stage of circuit testing. However, this
                        code will stay here as a kind of documentation and for later use.
                */
                auto test_crtc_registers() -> void {
                        Serial.println(F("CRTC Register Test"));
                        Serial.print(F("\tWriting "));
                        auto value{0x5A};
                        auto reg{HD6445::Register::SCREEN_1_START_ADDRESS_LOW_RW};
                        Serial.print(value);
                        Serial.print(F(" to register "));
                        Serial.print(reg);
                        m_crtc.write(reg, value);
                        auto read_back_value{m_crtc.read(reg)};
                        Serial.print(F(", read back "));
                        Serial.println(read_back_value);

                        Serial.print(F("\tWriting "));
                        value = 0xA5;
                        reg = HD6445::Register::SCREEN_1_START_ADDRESS_HIGH_RW;
                        Serial.print(value);
                        Serial.print(F(" to register "));
                        Serial.print(reg);
                        m_crtc.write(reg, value);
                        read_back_value = m_crtc.read(reg);
                        Serial.print(F(", read back (bits 6 and 7 always read 0) " ));
                        Serial.println(read_back_value);

                        const auto crtc_status{m_crtc.read(HD6445::Register::STATUS_RO)};
                        Serial.print(F("\tRead status "));
                        Serial.println(crtc_status);
                }

                auto populate_video_memory_cp850(const size_t mem_size) -> void {
                        Serial.print(F("Slowly populate VRAM with all CP850 characters"));
                        for (auto address{0U}; address < mem_size; address++) {
                                if (address % 50 == 0) {
                                        Serial.print(F("."));
                                }
                                const auto char_code{static_cast<Byte>(address % 256)};
                                m_video_ram.write(address, char_code);
                                delay(50);
                        }
                        Serial.println(F("OK"));
                }


                auto set_video_memory(const size_t mem_size, const char char_code) -> void {
                        Serial.print(F("Populate VRAM with given character..."));
                        for (auto address{0U}; address < mem_size; address++) {
                                m_video_ram.write(address, char_code);
                        }
                        Serial.println(F("OK"));
                }

                static constexpr auto HELLO{"    Hello, World!    "};
                static constexpr auto HELLO_SIZE{21U};

                auto display_some_text() -> void {
                        Serial.print(F("Display some text..."));
                        for (auto address{0U}; address < HELLO_SIZE; address++) {
                                const auto char_code{static_cast<Byte>(HELLO[address])};
                                m_video_ram.write(address, char_code);
                        }
                        Serial.println(F("OK"));
                }

                auto cycle_fonts_forever() -> void {
                        Serial.println(F("Slowly cycling fonts forever..."));
                        while (true) {
                                m_font.select(Font::SERIF);
                                delay(1000);
                                m_font.select(Font::SANS_SERIF);
                                delay(1000);
                                m_font.select(Font::COMPUTER);
                                delay(1000);
                                m_font.select(Font::TEMPLATE);
                                delay(1000);
                        }
                }

        private:
                auto write_some_bytes_and_read_back() -> void {
                        Serial.print(F("\tWriting some bytes beginning at 0x0000: "));
                        for (auto address{0U}; address < 16; address++) {
                                const auto value_to_be_written{address + 1};
                                if (address > 0) {
                                        Serial.print(F(", "));
                                }
                                Serial.print(value_to_be_written);
                                m_video_ram.write(address, value_to_be_written);
                        }
                        Serial.println();
                        Serial.print(F("\tReading some bytes beginning at 0x0000: "));
                        for (auto address{0U}; address < 16; address++) {
                                const auto value{m_video_ram.read(address)};
                                if (address > 0) {
                                        Serial.print(F(", "));
                                }
                                Serial.print(value);
                        }
                        Serial.println();
                }

                auto mem_test(const size_t mem_size, const Byte expected_value) -> bool {
                        Serial.print(F("\tVerifying all VRAM with "));
                        Serial.print(expected_value);
                        Serial.print(F("..."));
                        // TODO excluded address 0x0000 because it always fails --> find reason
                        for (auto address{1U}; address < mem_size; address++) {
                                m_video_ram.write(address, expected_value);
                        }
                        for (auto address{1U}; address < mem_size; address++) {
                                const auto actual_value{m_video_ram.read(address)};
                                if (actual_value != expected_value) {
                                        Serial.print(F("expected ["));
                                        Serial.print(address);
                                        Serial.print(F("] = "));
                                        Serial.print(expected_value);
                                        Serial.print(F(" but was "));
                                        Serial.println(actual_value);
                                        return false;
                                }
                        }
                        Serial.println(F("OK"));
                        return true;
                }

};
