#pragma once

#include "fundamental_definitions.hpp"

/**
 * The address bus is made up of two 74HC595 shift registers that are cascaded to form a 16 bit address bus.
 * The 16 bit address is shifted in, most significant bit first.
 * The address is taken over to the output latch once the complete address is shifted into the internal latch.
 * The address is available on the bus from here on.
 * 
 * This class controls all signals to manage the 74HC595 shift registers and to provide 16 bit addresses on the bus.
 * (Even though we are only using eleven bits.)
 */
class Address_Bus final {
        private:
                int m_output_latch_pin; // shifted data is taken over to output latch on positive edge
                int m_serial_clock_pin; // serial clock, positive edge
                int m_serial_data_pin;  // serial data

        public:
                Address_Bus(int latch_pin, int serial_clock_pin, int serial_data_pin) :
                        m_output_latch_pin{latch_pin}, m_serial_clock_pin{serial_clock_pin}, m_serial_data_pin{serial_data_pin} {
                        pinMode(m_output_latch_pin, OUTPUT);
                        pinMode(m_serial_clock_pin, OUTPUT);
                        pinMode(m_serial_data_pin, OUTPUT);
                }

                auto set(const Address address) -> void {
                        const auto low{static_cast<Byte>(address % 0x0100)};
                        const auto high{static_cast<Byte>((address - low) / 0x0100)};
                        digitalWrite(m_output_latch_pin, LOW);
                        shiftOut(m_serial_data_pin, m_serial_clock_pin, MSBFIRST, high);
                        shiftOut(m_serial_data_pin, m_serial_clock_pin, MSBFIRST, low);
                        digitalWrite(m_output_latch_pin, HIGH); // Take shifted data over to output latch
                }
};
