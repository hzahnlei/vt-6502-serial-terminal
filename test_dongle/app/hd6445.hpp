#pragma once

class HD6445 final {
        private:
                Address_Bus &m_address_bus;
                Data_Bus &m_data_bus;
                const int m_cs_pin;
                const int m_rd_pin;
                const int m_wr_pin;
                static constexpr Address REGISTER_ADDRESS_REGISTER{0x0000};
                static constexpr Address DATA_REGISTER{0x0001};

        public:
                HD6445(Address_Bus &address_bus, Data_Bus &data_bus, const int cs_pin, const int rd_pin, const int wr_pin) :
                        m_address_bus{address_bus}, m_data_bus{data_bus}, m_cs_pin{cs_pin}, m_rd_pin{rd_pin}, m_wr_pin{wr_pin} {
                        pinMode(m_cs_pin, OUTPUT);
                        deselect();
                        pinMode(m_rd_pin, OUTPUT);
                        pinMode(m_wr_pin, OUTPUT);
                        neither_read_nor_write();
                }

                /*
                        WO = write only, RO = read only, RW = read and write
                */
                enum Register {
                        HORIZONTAL_TOTAL_CHARACTERS_WO     =  0,
                        HORIZONTAL_DISPLAYED_CHARACTERS_WO =  1,
                        HORIZONTAL_SYNC_POSITION_WO        =  2,
                        SYNC_WIDTH_WO                      =  3,
                        VERTICAL_TOTAL_ROWS_WO             =  4,
                        VERTICAL_TOTAL_ADJUST_WO           =  5,
                        VERTICAL_DISPLAYED_ROWS_WO         =  6,
                        VERTICAL_SYNC_POSITION_WO          =  7,
                        INTERLACE_MODE_AND_SKEW_WO         =  8,
                        MAX_RASTER_ADDRESS_WO              =  9,
                        CURSOR_1_START_WO                  = 10,
                        CURSOR_1_END_WO                    = 11,
                        SCREEN_1_START_ADDRESS_HIGH_RW     = 12,
                        SCREEN_1_START_ADDRESS_LOW_RW      = 13,
                        CURSOR_1_ADDRESS_HIGH_RW           = 14,
                        CURSOR_1_ADDRESS_LOW_RW            = 15,
                        LIGHT_PEN_HIGH_RO                  = 16,
                        LIGHT_PEN_LOW_RO                   = 17,
                        SCREEN_2_START_POSITION_RW         = 18,
                        SCREEN_2_START_ADDRESS_HIGH_RW     = 19,
                        SCREEN_2_START_ADDRESS_LOW_RW      = 20,
                        SCREEN_3_START_POSITION_RW         = 21,
                        SCREEN_3_START_ADDRESS_HIGH_RW     = 22,
                        SCREEN_3_START_ADDRESS_LOW_RW      = 23,
                        SCREEN_4_START_POSITION_RW         = 24,
                        SCREEN_4_START_ADDRESS_HIGH_RW     = 25,
                        SCREEN_4_START_ADDRESS_LOW_RW      = 26,
                        VERTICAL_SYNC_POSITION_ADJUST_WO   = 27,
                        LIGHT_PEN_RASTER_RO                = 28,
                        SMOOTH_SCROLLING_RW                = 29,
                        CONTROL_1_WO                       = 30,
                        CONTROL_2_WO                       = 31, // write
                        STATUS_RO                          = 31, // read
                        CONTROL_3_WO                       = 32,
                        MEMORY_WIDTH_OFFSET_RW             = 33,
                        CURSOR_2_START_WO                  = 34,
                        CURSOR_2_END_WO                    = 35,
                        CURSOR_2_ADDRESS_HIGH_RW           = 36,
                        CURSOR_2_ADDRESS_LOW_RW            = 37,
                        CURSOR_1_WIDTH_RW                  = 38,
                        CURSOR_2_WIDTH_RW                  = 39,
                };

                auto write(const HD6445::Register register_id, const Byte value) -> void {
                        set_crtc_register_address(register_id);
                        write_to_crtc_data_register(value);
                }

                [[nodiscard]] auto read(const HD6445::Register register_id) -> Byte {
                        set_crtc_register_address(register_id);
                        return read_from_crtc_data_register();
                }

        private:
                inline auto select() -> void {digitalWrite(m_cs_pin, LOW);}
                inline auto deselect() -> void {digitalWrite(m_cs_pin, HIGH);}

                inline auto make_writable() -> void {
                        digitalWrite(m_rd_pin, HIGH);
                        digitalWrite(m_wr_pin, LOW);
                }
                inline auto make_readable() -> void {
                        digitalWrite(m_wr_pin, HIGH);
                        digitalWrite(m_rd_pin, LOW);
                }
                inline auto neither_read_nor_write() -> void {
                        digitalWrite(m_rd_pin, HIGH);
                        digitalWrite(m_wr_pin, HIGH);
                }

                auto set_crtc_register_address(const HD6445::Register register_id) -> void {
                        m_address_bus.set(REGISTER_ADDRESS_REGISTER);
                        //delayMicroseconds(1);
                        select();
                        //delayMicroseconds(1);
                        m_data_bus.write(register_id);
                        //delayMicroseconds(1);
                        make_writable();
                        //delayMicroseconds(1);
                        neither_read_nor_write();
                        //delayMicroseconds(1);
                        deselect();
                        //delayMicroseconds(1);
                }

                auto write_to_crtc_data_register(const Byte value) -> void {
                        m_address_bus.set(DATA_REGISTER);
                        //delayMicroseconds(1);
                        select();
                        //delayMicroseconds(1);
                        m_data_bus.write(value);
                        //delayMicroseconds(1);
                        make_writable();
                        //delayMicroseconds(1);
                        neither_read_nor_write();
                        //delayMicroseconds(1);
                        deselect();
                        //delayMicroseconds(1);
                }

                auto read_from_crtc_data_register() -> Byte {
                        m_address_bus.set(DATA_REGISTER);
                        //delayMicroseconds(1);
                        select();
                        //delayMicroseconds(1);
                        make_readable();
                        //delayMicroseconds(1);
                        const auto value{m_data_bus.read()};
                        deselect();
                        //delayMicroseconds(1);
                        neither_read_nor_write();
                        return value;
                }
};
