#pragma once

class IDT7132 final {
        private:
                Address_Bus &m_address_bus;
                Data_Bus &m_data_bus;
                const int m_ce_pin; // chip select, active low
                const int m_rw_pin; // r/w, high = read, low = write
                const int m_oe_pin; // output enabled, active high
        public:
                IDT7132(Address_Bus &address_bus, Data_Bus &data_bus, const int select_pin, const int rw_pin, const int oe_pin) :
                        m_address_bus{address_bus}, m_data_bus{data_bus}, m_ce_pin{select_pin}, m_rw_pin{rw_pin}, m_oe_pin{oe_pin} {
                        pinMode(m_ce_pin, OUTPUT);
                        deselect();
                        pinMode(m_rw_pin, OUTPUT);
                        pinMode(m_oe_pin, OUTPUT);
                        make_readable(); // IDT7132 data bus high-impedance
                        always_enable_output();
                }

                auto write(const Address address, const Byte value) -> void {
                        m_address_bus.set(address);
                        select();
                        make_writable();
                        m_data_bus.write(value);
                        deselect();
                }

                [[nodiscard]] auto read(const Address address) -> Byte {
                        m_address_bus.set(address);
                        select();
                        make_readable();
                        const auto value{m_data_bus.read()};
                        deselect();
                        return value;
                }


        private:
                inline auto select() -> void {digitalWrite(m_ce_pin, LOW);}
                inline auto deselect() -> void {digitalWrite(m_ce_pin, HIGH);}

                inline auto make_writable() -> void {digitalWrite(m_rw_pin, LOW);}
                inline auto make_readable() -> void {digitalWrite(m_rw_pin, HIGH);}

                inline auto always_enable_output() -> void {digitalWrite(m_oe_pin, LOW);}
};
