#pragma once

enum class Font {
        SERIF, SANS_SERIF, COMPUTER, TEMPLATE
};

class Font_Selector final {
        private:
                int m_font_lsb_pin; // Least significant bit
                int m_font_msb_pin; // Most significant bit
        public:
                Font_Selector(int font_lsb_pin, int font_msb_pin) : m_font_lsb_pin{font_lsb_pin}, m_font_msb_pin{font_msb_pin} {
                        pinMode(m_font_lsb_pin, OUTPUT);
                        pinMode(m_font_msb_pin, OUTPUT);
                        select(Font::SERIF);
                }

                auto select(const Font font) -> void {
                        switch (font) {
                                case Font::SERIF:
                                default:
                                        digitalWrite(m_font_lsb_pin, LOW);
                                        digitalWrite(m_font_msb_pin, LOW);
                                        return;
                                case Font::SANS_SERIF:
                                        digitalWrite(m_font_lsb_pin, HIGH);
                                        digitalWrite(m_font_msb_pin, LOW);
                                        return;
                                case Font::COMPUTER:
                                        digitalWrite(m_font_lsb_pin, LOW);
                                        digitalWrite(m_font_msb_pin, HIGH);
                                        return;
                                case Font::TEMPLATE:
                                        digitalWrite(m_font_lsb_pin, HIGH);
                                        digitalWrite(m_font_msb_pin, HIGH);
                                        return;
                        }
                }
};
