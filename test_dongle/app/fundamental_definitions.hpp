#pragma once

using Address = word;
using Byte = byte;

static constexpr int D2{2};
static constexpr int D3{3};
static constexpr int D4{4};
static constexpr int D5{5};
static constexpr int D6{6};
static constexpr int D7{7};
static constexpr int D8{8};
static constexpr int D9{9};
static constexpr int D10{10};
static constexpr int D11{11};
static constexpr int D12{12};
static constexpr int D13{13};
