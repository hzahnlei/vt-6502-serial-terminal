# Test Dongle

In order to accelerate development of hardware and software I constructed a "Test Dongle".
This is actually a circuit based on an Arduino Nano.
This circuit partially emulates the bus of an 6502 MPU.
The advantage is that it can quickly be reprogrammed via USB.
This way I can quickly try different HD6445 configurations thus accelerating the development.

74HC595 shift registers are used to expand the limited number of I/O pins in order to form a databus.
The software is always shifting 16 bits, even though the text video adapter only requires 11 bits (addressing 2kB of video Memory).
The access bits are simply ignored and not wired.

Be referred to the documentation of the [video adapter](../electronics/video/README.md) connector for a better understanding of the signals.

The software for the test dongle can be found here: [Arduino program](app/).

The following diagram shows the wiring of the Arduino Nano.
It is a version browser-viewable version of the original KiCad [schematics](schematics/).

![test dongle schematic](media/schematics.png "Test Dongle Schematic"){width=50%}

![front](media/front.png "Front Of Hand Wired Board"){width=50%}

![back](media/front.png "Back Of Hand Wired Board"){width=50%}

![connected](media/connected_to_video_board.png "Dongle Connected To Video Board"){width=50%}
