# Serial Terminal Software

## Attaching The Terminal To A Linux Computer

### Configuring The Linux Machine

A login shell needs to be configured to run on the the Linux computer's serial interface (`/dev/ttyS1` in my case) so that the terminal can be attached to it.
On my Kbuntu box (24.04) at least it is sufficient to add additional kernel command line arguments.
This will cause `systemd` to launch a service `serial-getty@ttyS1.service` next time the OS is booted.

Optionally, start by copying the original Grub configuration file:

```bash
sudo cp /etc/default/grub /etc/default/grub.ORIGINAL
```

Then, edit the original file:

```bash
sudo vi /etc/default/grub
```

In that file set the value for `GRUB_CMDLINE_LINUX_DEFAULT` like so:

```bash
GRUB_CMDLINE_LINUX_DEFAULT='quiet console=ttyS1,9600n8'
```

Activate the Grub settings by executing `sudo update-grub` and reboot.

### Connecting Terminal And Linux Computer

Now connect the serial terminal via RS-232 cable to the computers serial port (`/dev/ttyS1`).

### Starting The Terminal Session

The highest posibility for a successfull connection will be to turn on the terminal first.
Then start the Linux computer.
At system initialisation `systemd` will start the service `serial-getty@ttyS1.service` and a login screen will be displayed by the already waiting terminal.
It will take a few seconds for the cathode ray tube to heat up and to show an image.
Therefore, wait until you see the terminal's cursor before booting the Linux box.
After a short period of time, you should see a simple login screen.
Enter user name and password - et voilà.

No connection might be established, if the Linux box was already up by the time you connected the terminal.
In that case restart the service and shortly after you should see the login prompt.

```bash
sudo systemctl restart serial-getty@ttyS1.service
```
