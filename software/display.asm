;==============================================================================
;
; The VT-6502 Serial Terminal Application
; (c) 2024 by Holger Zahnleiter, all rights reserved
;
; IN THIS FILE:
; Routines for clearing the screen, printing characters on screen, and so on.
;
;==============================================================================

	; .in r6501_mcu.asm
	; .in hd6445_crtc.asm


; Beginning at  memory location 0x0000 there is  2kB of dual port RAM (IDT7132,
; 2k×8).  This is used as our frame buffer.  We skip the first page (256 bytes)
; as this page is eclipsed by the R6501's internal RAM and hardware control re-
; gisters.
SHARED_MEMORY_START_ADDR = 0x0000
VRAM_START_ADDR          = SHARED_MEMORY_START_ADDR+256
VRAM_START_ADDR_LO       = VRAM_START_ADDR&0x00FF
VRAM_START_ADDR_HI       = VRAM_START_ADDR>>8

COLUMN_COUNT = 80
ROW_COUNT    = 15

VRAM_SIZE        = ROW_COUNT*COLUMN_COUNT
VRAM_END_ADDR    = VRAM_START_ADDR+VRAM_SIZE
VRAM_END_ADDR_LO = VRAM_END_ADDR&0x00FF
VRAM_END_ADDR_HI = VRAM_END_ADDR>>8

LAST_CRSR_POS    = VRAM_END_ADDR-COLUMN_COUNT
LAST_CRSR_POS_LO = LAST_CRSR_POS&0x00FF
LAST_CRSR_POS_HI = LAST_CRSR_POS>>8

FIRST_LINE_ADDR    = VRAM_START_ADDR
FIRST_LINE_ADDR_LO = FIRST_LINE_ADDR&0x00FF
FIRST_LINE_ADDR_HI = FIRST_LINE_ADDR>>8

SECOND_LINE_ADDR    = VRAM_START_ADDR+COLUMN_COUNT
SECOND_LINE_ADDR_LO = SECOND_LINE_ADDR&0x00FF
SECOND_LINE_ADDR_HI = SECOND_LINE_ADDR>>8

;------------------------------------------------------------------------------
; Variables of this "module" in RAM (page zero). Need to be defined upfront, so
; that SBASM picks zero page addressing instead of absolute addressing.
;------------------------------------------------------------------------------

	.sm ram

FRAME_BUF_PTR:		; Shared by CLEAR_SCREEN and SCROLL_UP
	.dw 0
FRAME_BUF_PTR_LO = FRAME_BUF_PTR
FRAME_BUF_PTR_HI = FRAME_BUF_PTR+1

FONT__SERIF      = 0b00000000
FONT__SANS_SERIF = 0b00000100
FONT__COMPUTER   = 0b00001000
FONT__TEMPLATE   = 0b00001100
FONT__MASK       = 0b11110011
	.sm ram
FONT:
	.db 0

CURSOR_ADDR:
	.dw 0
CURSOR_ADDR_LO = CURSOR_ADDR
CURSOR_ADDR_HI = CURSOR_ADDR+1


POS_FROM_RIGHT:
	.db 0

CPY_FROM_PTR:
	.dw 0
CPY_FROM_PTR_LO = CPY_FROM_PTR
CPY_FROM_PTR_HI = CPY_FROM_PTR+1

CPY_TO_PTR:
	.dw 0
CPY_TO_PTR_LO = CPY_TO_PTR
CPY_TO_PTR_HI = CPY_TO_PTR+1


	.sm code

;------------------------------------------------------------------------------
; THIS ROUTINE:
; Initializes the CRTC character display.  Register numbers and associated con-
; figuration values are read from a table in ROM.
;
; This works with a 14.7456MHzMHz crystal driving the HD6445 CRTC-II instead of
; the original 15.33MHz. The character display will be 80×15 characters of 8×16
; pixels.  This is less than the  original resolution of the WP-1.  However, it
; keeps our visible display  away from the tube's borders  thus avoiding pillow
; effects.
;------------------------------------------------------------------------------
INIT_DISPLAY_HARDWARE:
	ldx #0
.CRTC_INIT_LOOP
	lda .CRTC_REGISTER_NUMBER,x	; Setup desired register number in
	sta CRTC_ADDR_REGISTER		; CRTC's address register
	lda .CRTC_REGISTER_INIT_VALUE,x	; Write init value to CRTC's register
	sta CRTC_DATA_REGISTER		; pointed to by address register
	inx
	cpx #21
	bne .CRTC_INIT_LOOP

	lda #0b00000011			; All outputs on Port B set to zero,
	sta PORT_B			; hence selecting font #1, except for
					; B0/B1 which are used as keyboard
					; inputs
	rts

.CRTC_REGISTER_NUMBER
	.db CRTC_REG_HOR_TOTAL_CHARS
	.db CRTC_REG_HOR_DISP_CHARS
	.db CRTC_REG_HOR_SYNC_POS
	.db CRTC_REG_SYNC_WIDTH
	.db CRTC_REG_VERT_TOTAL_CHARS
	.db CRTC_REG_VERT_TOTAL_ADJUST
	.db CRTC_REG_VERT_DISP_CHARS
	.db CRTC_REG_VERT_SYNC_POS
	.db CRTC_REG_INTERLACE_AND_SKEW
	.db CRTC_REG_MAX_RASTER_ADDR
	.db CRTC_REG_CURSOR_1_START
	.db CRTC_REG_CURSOR_1_END
	.db CRTC_REG_SCRN_1_START_ADDR_HI
	.db CRTC_REG_SCRN_1_START_ADDR_LO
	.db CRTC_REG_CURSOR_1_ADDR_HI
	.db CRTC_REG_CURSOR_1_ADDR_LO
	.db CRTC_REG_SMOOTH_SCROLLING 
	.db CRTC_REG_CONTROL_1
	.db CRTC_REG_CONTROL_2
	.db CRTC_REG_CONTROL_3
	.db CRTC_REG_MEM_WIDTH_OFFSET

.CRTC_REGISTER_INIT_VALUE
	.db 112				; Total columns (screen width)
	.db COLUMN_COUNT		; Visible columns
	.db 86
	.db 0b11111111
	.db ROW_COUNT			; Total rows (screen height)
	.db 4
	.db 15				; Visible rows
	.db 15
	.db 0b01010000
	.db 15
	.db 0b01100000
	.db 16
	.db VRAM_START_ADDR_HI		; First memory address
	.db VRAM_START_ADDR_LO
	.db VRAM_START_ADDR_HI		; Cursor starts at address
	.db VRAM_START_ADDR_LO
	.db 0
	.db 0b00101000
	.db 0b00000000
	.db 0b01000000
	.db 0


;------------------------------------------------------------------------------
; THIS ROUTINE:
; Initializes the memory variables assiciated with this display module.
;------------------------------------------------------------------------------
INIT_DISPLAY_SOFTWARE:
	lda #0b00000000			; Remember in software: We picked font
	sta FONT			; #1 in hardware initialization

	lda #VRAM_START_ADDR_LO		; Cursor at row 0, column 0, which is
	sta CURSOR_ADDR_LO		; equivalent to the starting address of
	lda #VRAM_START_ADDR_HI		; the framebuffer
	sta CURSOR_ADDR_HI

	lda #COLUMN_COUNT		; Cursor starts at the very left side
	sta POS_FROM_RIGHT		; of the screen, therefore 80 chars
					; from the right
	rts

;------------------------------------------------------------------------------
; THIS ROUTINE:
; Fills frame buffer with blank characters thereby "clearing" the screen.
;
; The screen has 80×15 characters,  that makes 1,200 characters in total.  This
; requires 4.6875 outer loops (x) of 256 inner loops (y). In other words, after
; 4 complete outer loops, we need another loop of 176 steps in order to comple-
; tely  clear the screen  without writing beyond the  visible screen in memory.
; This way,  we may use the remaining bytes behind the frame buffer for storing
; program data.
;------------------------------------------------------------------------------
CLEAR_SCREEN:
	lda #VRAM_START_ADDR_LO		; Initialize variable in page zero to
	sta FRAME_BUF_PTR_LO		; point to beginning of frame buffer
	lda #VRAM_START_ADDR_HI
	sta FRAME_BUF_PTR_HI

	lda #' '
	ldx #0
	ldy #0
.LOOP
	sta (FRAME_BUF_PTR),y
	iny
	bne .LOOP
	inc FRAME_BUF_PTR_HI
	inx
	cpx #4
	bne .LOOP
.REMAINDER
	sta (FRAME_BUF_PTR),y
	iny
	cpy #176
	bne .REMAINDER

	rts


;------------------------------------------------------------------------------
; THIS ROUTINE:
; Activates one out of four fonts for display.
;
; Modifies bits two and three of port B in order to pick one out of four fonts.
; Actually these two bits are the  most significant bits applied to the charac-
; ter ROM.
;
; Bits two and three of  port B are used  to read the keyboard.  Therefore care
; needs to be taken so that display code does not screw up keyboard code.
;
; The code of the font is expected to be available in register Y. The code must
; be either FONT__SERIF, FONT__SANS_SERIF, FONT__COMPUTER, or FONT__TEMPLATE.
;------------------------------------------------------------------------------
PICK_FONT_Y:
	lda FONT		; Clear font bits in font selector variable in
	and #FONT__MASK		; memory
	sta FONT

	tya			; Apply new font bits to font selector variable
	ora FONT 		; in memory
	sta FONT

	sta PORT_B		; Apply new font to hardware font selector port

	rts

;------------------------------------------------------------------------------
; THIS ROUTINE:
; Prints a byte (character) on screen.
; The character gets printed  at the current cursor position.  The cursor posi-
; tion is updated accordingly.  The screen is getting scrolled up  by one line,
; if required.
;
; The character to be printed is expected in register A.
;------------------------------------------------------------------------------
PRINT_BYTE:
	cmp #ASCII__BEL			; Register A = BEL?
	beq .HANDLE_BEL
	cmp #ASCII__CR			; Register A = CR?
	beq .HANDLE_CR
	cmp #ASCII__TAB			; Register A = TAB?
	beq .HANDLE_TAB
.HANDLE_PRINTABLE_CHAR
	ldy #0				; Print byte in register A at current
	sta (CURSOR_ADDR),y		; cursor position

	jmp ADVANCE_ONE_CHAR
	;;; rts done by ADVANCE_ONE_CHAR
.HANDLE_BEL
	jmp START_BELL_BEEP		; Silenced in main after some time
	;;; rts	done by	START_BELL_BEEP		
.HANDLE_CR
	jmp ADVANCE_ONE_LINE
	;;; rts done by ADVANCE_ONE_LINE
.HANDLE_TAB
	ldx #0				; Print 8 consecutive blanks at current
					; cursor position
.LOOP_TAB
	lda #' '
	ldy #0
	sta (CURSOR_ADDR),y
	txa
	pha
	jsr ADVANCE_ONE_CHAR
	pla
	tax
	inx
	cpx #8
	bne .LOOP_TAB
	rts


;------------------------------------------------------------------------------
; THIS ROUTINE:
; Advances the cursor to the right  by one character.  Applies to the situation
; where a single,  printable character has been printed.  Not applicable to non
; printable characters like CR, BS etc.
;------------------------------------------------------------------------------
ADVANCE_ONE_CHAR:
	clc			; Compute next cursor position
	lda CURSOR_ADDR_LO
	adc #1
	sta CURSOR_ADDR_LO
        bcc .MOVE_CRS_BY_ONE            
        inc CURSOR_ADDR_HI
.MOVE_CRS_BY_ONE
	dec POS_FROM_RIGHT	; Cursor has moved to the right by one char
	bne .DONE		
.CHECK_END_OF_LINE
	lda #COLUMN_COUNT	; The cursor is at the very left of the screen
	sta POS_FROM_RIGHT	; now, so adjust POS_FROM_RIGHT
.CHECK_BOTTOM_OF_SCREEN
	lda CURSOR_ADDR_HI	; Cursor addr ≥ end of screen?
	cmp #VRAM_END_ADDR_HI
	bcc .DONE		; NO, Cursor addr < end of screen
	bne .DONE		; NO, Cursor addr ≠ end of screen
.CHECK_LO
	lda CURSOR_ADDR_LO
	cmp #VRAM_END_ADDR_LO
	bcc .DONE		; NO, Cursor addr < end of screen
.SCROLL
	jsr SCROLL_UP
.DONE
	jmp UPDATE_HW_CRSR	; As computed here or by SCROLL_UP
	;;; rts done by UPDATE_HW_CRSR


;------------------------------------------------------------------------------
; THIS ROUTINE:
; Scrolls the screen up by one line.  It also positions the cursor  at the left
; most position of the last line.
;------------------------------------------------------------------------------
SCROLL_UP
	; First put the cursor to the left most position of the bottom line. Be
	; advised:  This only updates the memory variable.  The actual hardware
	; cursor has to be updated by the caller of this routine.
	lda #LAST_CRSR_POS_LO	
	sta CURSOR_ADDR_LO	
	lda #LAST_CRSR_POS_HI
	sta CURSOR_ADDR_HI
	
	; Then actually scroll screen up  by one line by performing a block co-
	; py.
	lda #SECOND_LINE_ADDR_LO	; Source pointer
	sta CPY_FROM_PTR_LO
	lda #SECOND_LINE_ADDR_HI
	sta CPY_FROM_PTR_HI

	lda #FIRST_LINE_ADDR_LO		; Destination pointer
	sta CPY_TO_PTR_LO
	lda #FIRST_LINE_ADDR_HI
	sta CPY_TO_PTR_HI

	ldx #0				; Page index
	ldy #0				; Byte within page index
.LOOP
	lda (CPY_FROM_PTR),y
	sta (CPY_TO_PTR),y
	iny				; Next byte
	bne .LOOP
	inc CPY_FROM_PTR_HI		; Next page
	inc CPY_TO_PTR_HI
	inx
	cpx #4
	bne .LOOP
.REMAINDER
	lda (CPY_FROM_PTR),y
	sta (CPY_TO_PTR),y
	iny
	cpy #176-COLUMN_COUNT
	bne .REMAINDER

	; Finally clear last line.
	lda #LAST_CRSR_POS_LO
	sta FRAME_BUF_PTR_LO
	lda #LAST_CRSR_POS_HI
	sta FRAME_BUF_PTR_HI
	ldy #COLUMN_COUNT-1
	lda #' '
.CLEAR_LAST_LN
	sta (FRAME_BUF_PTR),y
	iny
	bne .CLEAR_LAST_LN
	sta (FRAME_BUF_PTR),y

	rts


;------------------------------------------------------------------------------
; THIS ROUTINE:
; The software first computes  the (new) cursor position  in a memory variable.
; This routine makes this (new) cursor position effective by transferring it to
; the HD6445 cursor registers.  The hardware cursor  will then be  displayed at
; the new position.
;------------------------------------------------------------------------------
UPDATE_HW_CRSR:
	ldy #CRTC_REG_CURSOR_1_ADDR_HI	; Tell CRTC to (re)position the cursor
	ldx CURSOR_ADDR_HI
	jsr SET_CRTC_REGISTER
	ldy #CRTC_REG_CURSOR_1_ADDR_LO
	ldx CURSOR_ADDR_LO
	jmp SET_CRTC_REGISTER
	;;;rts - not required, SET_CRTC_REGISTER will rts for us


;------------------------------------------------------------------------------
; THIS ROUTINE:
; Puts the cursor to the beginning of the next line.  The screen is scrolled up
; by one line, if the bottom of the screen has been reached.
;------------------------------------------------------------------------------
ADVANCE_ONE_LINE:
	clc
	lda CURSOR_ADDR_LO
	adc POS_FROM_RIGHT
	sta CURSOR_ADDR_LO
	bcc .CHECK_BOTTOM_OF_SCREEN
	inc CURSOR_ADDR_HI
.CHECK_BOTTOM_OF_SCREEN
	lda CURSOR_ADDR_HI	; Cursor addr ≥ end of screen?
	cmp #VRAM_END_ADDR_HI
	bcc .DONE		; NO, Cursor addr < end of screen
	bne .DONE		; NO, Cursor addr ≠ end of screen
.CHECK_LO
	lda CURSOR_ADDR_LO
	cmp #VRAM_END_ADDR_LO
	bcc .DONE		; NO, Cursor addr < end of screen
.SCROLL	
	jsr SCROLL_UP	
.DONE
	lda #COLUMN_COUNT
	sta POS_FROM_RIGHT

	jmp UPDATE_HW_CRSR	; As computed here or by SCROLL_UP
	;;; rts done by UPDATE_HW_CRSR
