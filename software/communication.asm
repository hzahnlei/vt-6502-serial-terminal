;==============================================================================
;
; The VT-6502 Serial Terminal Application
; (c) 2024 by Holger Zahnleiter, all rights reserved
;
; IN THIS FILE:
; Routines for receiving characters from and  sending characters to a host com-
; puter via serial RS232 interface.
;
;==============================================================================

;------------------------------------------------------------------------------
; Variables of this "module" in RAM (page zero). Need to be defined upfront, so
; that SBASM picks zero page addressing (faster) instead of absolute addressing
; (slower).
;------------------------------------------------------------------------------

	.sm ram

; A small buffer  for bytes received  via serial interface.  Buffering might be
; required as some tasks,  for example scrolling the screen,  consume some more
; time.  During that time, bytes might be received in quick succession and get-
; ting lost as the MCU is busy with other tasks.
INPUT_BUF:
	.db 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
	.db 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
INPUT_BUF_HEAD:
	.db 0
INPUT_BUF_TAIL:
	.db 0
INPUT_BUF_SIZE = INPUT_BUF_HEAD-INPUT_BUF

; The buffer might overflow,  if its content is not consumed fast enough.  This
; flag indicates a good or bad buffer.
INPUT_BUFFER_PUT_STATUS:
	.db 0
INPUT_BUFFER_PUT_STATUS__OK       = 0
INPUT_BUFFER_PUT_STATUS__OVERFLOW = 1


	.sm code

;------------------------------------------------------------------------------
; THIS ROUTINE:
; Initializes the serial communications hardware of the R6501.  This is made up
; of an UART and  Counter A for a Baud rate generator.  The R6501 is clocked at
; 1.8432MHZ. The configuration is 8N2 at 9.600 bps.
;------------------------------------------------------------------------------
INIT_COMMUNICATION_HARDWARE:
	lda #0b11000000		; Enable XMTR/RCVR, async, 8N2
	sta SCCR

	lda #0x00		; Counter A (frequency divider) latch (re-load
	sta ULA			; value) set to 0x000B for standard Baud rate
	lda #0x0B		; of 9.600 bps (assuming MCU clocked at
	sta LLA			; 1.8432MHz)

	lda IER			; Enable interrupt that fires, if byte is
	ora #IER__RCVR_FLAG	; received via serial interface
	sta IER

	rts


;------------------------------------------------------------------------------
; THIS ROUTINE:
; Sets up a ring buffer for storing bytes received via serial interface.
;------------------------------------------------------------------------------
INIT_COMMUNICATION_SOFTWARE:
CLEAR_INPUT_BUFFER:		; An alias for better readability in code
	lda #0			; Input buffer pointer for write and read are
	sta INPUT_BUF_HEAD	; both pointing to the beginning of the buffer
	sta INPUT_BUF_TAIL
	
	lda #INPUT_BUFFER_PUT_STATUS__OK
	sta INPUT_BUFFER_PUT_STATUS
	
	rts


;------------------------------------------------------------------------------
; THIS ROUTINE:
; Sends keys pressed via serial interface to host computer.
; Expects character value in register Y.
;------------------------------------------------------------------------------
SERIAL_SEND_CHAR:
	pha			; Save register A
.CHECK_HW_RCV_BUF
	lda SCSR		; Loop for as long as transmit buffer not ready
	and #SCSR__TDRE 	
	beq .CHECK_HW_RCV_BUF
	sty SCDAT		; Buffer is ready, therefore send char in Y
				; register
	pla			; Restore register A
	rts


;------------------------------------------------------------------------------
; THIS ROUTINE:
; Reads character via serial interface from host computer.
; Be advised,  there is no such a routine contained herein,  even though it be-
; longs into this module.  For practical reasons, reading the byte received and
; puting it into a buffer is directly implemented in the interrupt service rou-
; tine (ISR). See main.asm for defails.
;------------------------------------------------------------------------------
; SERIAL_READ_CHAR:
; 	rts


;------------------------------------------------------------------------------
; THIS ROUTINE:
; Put one byte, received via serial interface, into ring buffer.
; This function is to be called from within  the interupt service routine (ISR)
; of the serial receiver. Interrupt are inhibited already, therefore no inhibi-
; ting of interrupts to protect buffer access required here.
;
; The current byte, received from the serial interface, is expected in register
; A.
; After this routine  has endend,  INPUT_BUFFER_PUT_STATUS  will signal whether
; the byte was aktually buffered,  or a buffer overflow has hindered storage of
; the byte.
;------------------------------------------------------------------------------
INPUT_BUFFER_PUT:
	ldy INPUT_BUF_HEAD	; Remember current head

	ldx INPUT_BUF_HEAD	; Advance head by one entry
	inx

	cpx #INPUT_BUF_SIZE	; Check if head out of bounds
	bcc .CHECK_OVERFLOW
.WRAP_HEAD_AROUND
	ldx #0			; Head within bounds again
.CHECK_OVERFLOW
	cpx INPUT_BUF_TAIL	; If head == tail, then error
	beq .BUFFER_OVERFLOW

	stx INPUT_BUF_HEAD	; Remember new head and consume byte from
	sta INPUT_BUF,y		; buffer at current head
	lda #INPUT_BUFFER_PUT_STATUS__OK
	sta INPUT_BUFFER_PUT_STATUS

	rts
.BUFFER_OVERFLOW
	lda #INPUT_BUFFER_PUT_STATUS__OVERFLOW
	sta INPUT_BUFFER_PUT_STATUS
	
	rts


;------------------------------------------------------------------------------
; THIS ROUTINE:
; Get one byte, received via serial interface before, from ring buffer.
; Byte taken from the buffer will be available in register A.
; Register X will tell whether the buffer contained any data. Register A
; is valid only if register X=1.
;------------------------------------------------------------------------------
INPUT_BUFFER_GET:
	sei			; Prevent interrupts, only one side (reader or
				; writer) must access the buffer at any time
	ldx INPUT_BUF_TAIL	; Check if the buffer contains any data
	cpx INPUT_BUF_HEAD	; head == tail, then buffer empty
	beq .BUFFER_EMPTY
.BUFFER_NOT_EMPTY
	lda INPUT_BUF,x		; Take data from buffer

	inx			; Advance tail by one entry
	cpx #INPUT_BUF_SIZE	; Check if tail out of bounds
	bcc .RETURN_BYTE
.WRAP_TAIL_AROUND
	ldx #0			; Tail within bounds again
.RETURN_BYTE
	stx INPUT_BUF_TAIL
	ldx #INPUT_BUFFER_GET__FOUND
	jmp .DONE
.BUFFER_EMPTY
	ldx #INPUT_BUFFER_GET__EMPTY
.DONE
	cli			; Allow interrupts again
	rts

INPUT_BUFFER_GET__FOUND = 1
INPUT_BUFFER_GET__EMPTY = 0
