;==============================================================================
;
; The VT-6502 Serial Terminal Application
; (c) 2024 by Holger Zahnleiter, all rights reserved
;
; IN THIS FILE:
; Routines for making an audible beep sound via the speaker.
;
;==============================================================================

	.sm code

;------------------------------------------------------------------------------
; THIS ROUTINE:
; Initializes Counter B for generating the beeping frequency.
;
; Counter B  initialization sequence for  Assymetrical Pulse Generation Mode as
; described in the datasheet (p. 3-122).  This actually is a symmetrical signal
; as both P and D have the same value.
; The shape of the beep pulses generated  by Counter B is loaded to the corres-
; ponding latches already in this routine. Even though there will be no audible
; beep yet. The beeping gets activated/deactivated later by switching Counter B
; modes.  (Assymetrical Pulse Generation Mode for beeping,  Interval Timer Mode
; for silence.)
;------------------------------------------------------------------------------
INIT_AUDIO_HARDWARE:
	lda #0x00	; Pulse width P
	sta LLBC
	lda #0x08
	sta ULC
	lda #0x0	; Duration D
	sta LLBC
	lda #0x08
	sta ULB

	rts


;------------------------------------------------------------------------------
; THIS ROUTINE:
; Delays for about 0.4sec.
;
; A primitive busy delay. The time can be approximated by assuming 10 cycles
; for the loop. This is repeated 64k times at 1.8432MHz.
;------------------------------------------------------------------------------
DELAY:
	ldx #0xff	; Set counters for busy-waiting style delay
	ldy #0xff	; 256×256=65.536 repetitions in total
.LOOP
	dex		; Inner delay loop
        bne .LOOP
        dey		; Outer delay loop
        bne .LOOP
        
	rts


;------------------------------------------------------------------------------
; THIS ROUTINE:
; Let the speaker beep at startup to indicate terminal works.
;------------------------------------------------------------------------------
INITIAL_BEEP:
	lda MCR		; Counter B in Assymetrical Pulse Generation Mode to
	ora #0b00000100	; generate pulses at Port A5 (CNTB)
	sta MCR

	jsr DELAY	; Keep beeping for some time
	; FIXME reaktivate
	; jsr DELAY
	; jsr DELAY
	; jsr DELAY
	
	lda MCR		; Counter B back in Interval Timer Mode to end the
	and #0b11110011	; beeping
	sta MCR
	
	rts


STOP_BEEPING:
	lda MCR		; Counter B back in Interval Timer Mode to end the
	and #0b11110011	; beeping
	sta MCR

	rts


START_BELL_BEEP:
	lda #0x00	; Set higher pitch, relative to error beep
	sta LLBC
	lda #0x08
	sta ULC
	lda #0x0
	sta LLBC
	lda #0x08
	sta ULB

	lda MCR		; Counter B in Assymetrical Pulse Generation Mode to
	ora #0b00000100	; generate pulses at Port A5 (CNTB)
	sta MCR

	rts


START_ERR_BEEP:
	lda #0x00	; Set lower pitch, relative to bell beep
	sta LLBC
	lda #0x18
	sta ULC
	lda #0x0
	sta LLBC
	lda #0x18
	sta ULB

	lda MCR		; Counter B in Assymetrical Pulse Generation Mode to
	ora #0b00000100	; generate pulses at Port A5 (CNTB)
	sta MCR

	rts