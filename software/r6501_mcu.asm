;==============================================================================
;
; The VT-6502 Serial Terminal Application
; (c) 2024 by Holger Zahnleiter, all rights reserved
;
; IN THIS FILE:
; R6501 hardware control register addresses and flags.
;
;==============================================================================


;---- Hardware stack ----
STACK_START_ADDR = 0x00FF	; Initial start address of SP in page zero

;---- I/O ports ----
PORT_B = 0x0001	; I/O Port B
PORT_C = 0x0002	; I/O Port C
PORT_D = 0x0003	; I/O Port D

;---- Interrupts ----
IFR            = 0x0011		; Interrupt Flag Register
IFR__RCVR_FLAG = 0b01000000	; 1, if byte received via serial interface.
				; Cleared when SCSR0-3 are cleared, which is
				; the case when SCDAT is read

IER            = 0x0012		; Interrupt Enable Register
IER__RCVR_FLAG = IFR__RCVR_FLAG	; Enable interrupt on serial receive 

;---- Bus and peripherals control ----
MCR = 0x0014	; Mode Control Register

;---- Serial interface ----
SCCR       = 0x0015	; Serial Communication Control Register
SCSR       = 0x0016	; Serial Communication Status Register
SCSR__RDRF = 0b00000001	; SCSR flag Receiver Data Register Full
SCSR__TDRE = 0b01000000	; SCSR flag Transmitter Data Register Empty
SCDAT      = 0x0017	; Serial Communication Data Register. Reading this
			; register will clear the receiver interrupt flag

;---- Counter A (used as Baud rate generator) ----
LLA = 0x0018	; Lower Latch Counter A
ULA = 0x0019	; Upper Latch Counter A

;---- Counter B (used for beeping) ----
LLBC = 0x001C	; Lower Latch B/C Counter B
ULC  = 0x001D	; Upper Latch C Counter B
ULB  = 0x001E	; Upper Latch B Counter B
