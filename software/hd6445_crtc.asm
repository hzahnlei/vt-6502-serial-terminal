;==============================================================================
;
; The VT-6502 Serial Terminal Application
; (c) 2024 by Holger Zahnleiter, all rights reserved
;
; IN THIS FILE:
; HD6445 CRTC-II definitions, register addresses and routines.
;
;==============================================================================

; The CRTC's control registers are mapped to memory location 0x1000. The HD6445
; actually only has two registers exposed to the MCU: address register and data
; register. The actual registers are not directly accessible by the MCU but are
; read from/written to  by means of the address/data registers.  One first lets
; the  address register point to one of  the actual control registers  and then
; reads from or writes to the data register.
CRTC_START_ADDR               = 0x1000			; HD6445 CRTC-II
CRTC_ADDR_REGISTER            = CRTC_START_ADDR+0
CRTC_DATA_REGISTER            = CRTC_START_ADDR+1
CRTC_REG_HOR_TOTAL_CHARS      = 0
CRTC_REG_HOR_DISP_CHARS       = 1
CRTC_REG_HOR_SYNC_POS         = 2
CRTC_REG_SYNC_WIDTH           = 3
CRTC_REG_VERT_TOTAL_CHARS     = 4
CRTC_REG_VERT_TOTAL_ADJUST    = 5
CRTC_REG_VERT_DISP_CHARS      = 6
CRTC_REG_VERT_SYNC_POS        = 7
CRTC_REG_INTERLACE_AND_SKEW   = 8
CRTC_REG_MAX_RASTER_ADDR      = 9
CRTC_REG_CURSOR_1_START       = 10
CRTC_REG_CURSOR_1_END         = 11
CRTC_REG_SCRN_1_START_ADDR_HI = 12
CRTC_REG_SCRN_1_START_ADDR_LO = 13
CRTC_REG_CURSOR_1_ADDR_HI     = 14
CRTC_REG_CURSOR_1_ADDR_LO     = 15
CRTC_REG_SMOOTH_SCROLLING     = 29
CRTC_REG_CONTROL_1            = 30
CRTC_REG_CONTROL_2            = 31
CRTC_REG_CONTROL_3            = 32
CRTC_REG_MEM_WIDTH_OFFSET     = 33


	.sm code

;------------------------------------------------------------------------------
; THIS ROUTINE:
; Sets a given HD6445 hardware register to a given value.
;
; Register number is expected in MCU register Y.
; Value is expected in MCU register X.
;
; This routine, especially disabeling  interrupts and the "nop", is modeled af-
; ter the original WP-1 routine.
;------------------------------------------------------------------------------
SET_CRTC_REGISTER:
	sei				; No interrupts
	sty CRTC_ADDR_REGISTER		; Select CRTC register Y
	nop			
	stx CRTC_DATA_REGISTER		; Write value X to selected register
	cli				; Allow interrupts
	rts
