;==============================================================================
;
; The VT-6502 Serial Terminal Application
; (c) 2024 by Holger Zahnleiter, all rights reserved
;
; IN THIS FILE:
; Routines for reading the keyboard.
;
;==============================================================================

;------------------------------------------------------------------------------
; Variables of this "module" in RAM (page zero). Need to be defined upfront, so
; that SBASM picks zero page addressing instead of absolute addressing.
;------------------------------------------------------------------------------

	.sm ram

; Port B bits 0 and 1 are the inputs from the keyboard matrix to the micro con-
; troller. Be advised: Bits 2 and 3 of that same port are outputs to select one
; of four fonts  to be displayed.  So, port B needs to be handled  with care so
; that keyboard handling and font control do not screw up each other.
MASK_KB_IN_0 = 0b00000001	; "Regular" keys as well as some modifier keys
				; are connected to port B0
MASK_KB_IN_1 = 0b00000010	; Most modifier keys are connected to port B1

; Key read from keyboard. Only valid if KEY_STATUS=KEY_STATUS__PRESSED
KEY__NONE = 0x00		; No key pressed
KEY:
	.db KEY__NONE

; The keyboard matrix  is made up of  eight rows and eight columns.  This makes
; for 64 keys.  (Not all of the crossings in the matrix  are actually connected
; to a key. Mist of them are, however.)
KEY_COUNT            = 64
LAST_KEY_TABLE_ENTRY = KEY_COUNT-1

; Modifier keys pressed, if any.  The modifier keys are only regarded in combi-
; nation with  pressed non-modifier keys  (KEY ≠ KEY__NONE).  Multiple modifier
; keys may be pressed at the same time.
MOD_KEY__NONE  = 0b00000000	; No modifier key pressed
MOD_KEY__CTRL  = 0b00000001	; CTRL key was pressed
MOD_KEY__ESC   = 0b00000010	; ESC key was pressed
MOD_KEY__SHIFT = 0b00000100	; SHIFT key was pressed
MOD_KEY__ATARI = 0b00001000	; Atari key was pressed
MOD_KEY__CAPS  = 0b00010000	; CAPS LOWER key was pressed
MOD_KEY:
	.db MOD_KEY__NONE


	.sm code

MOD_KEY_COL_ROW_ATARI = 0b00000011
MOD_KEY_COL_ROW_SHIFT = 0b00111110

;------------------------------------------------------------------------------
; THIS ROUTINE:
; Sets up variables (in page zero) of the keyboard "module" to initial values.
;------------------------------------------------------------------------------
INIT_KEYBOARD_SOFTWARE:
	lda #KEY__NONE
	sta KEY

	lda #MOD_KEY__NONE
	sta MOD_KEY
	
	rts


;------------------------------------------------------------------------------
; THIS ROUTINE:
; Scan keyboard matrix  for key presses.  First we scan for  regular keys.  Our
; scan ends with  the first regular key found.  Then we scan for modifier keys,
; if a regular key has been pressed.
;
; Rows and columns of the keyboard matrix are selected via Port D.
;
; Port D bit | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 |
;                      !   !   !   !   !   !
;                      !   !   !   !   !   +---- KbRowSel0 output
;                      !   !   !   !   +-------- KbRowSel1 output
;                      !   !   !   +------------ KbRowSel2 output
;                      !   !   +---------------- KbColSel0 output
;                      !   +-------------------- KbColSel1 output
;                      +------------------------ KbColSel2 output
;
; The keys are read via  Port B used as an input port.  Please note that Port B
; also provides two bits of output for selecting one out of four fonts.
;
; Port B bit | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 |
;                              !   !   !   !
;                              !   !   !   +---- KbIn0 input
;                              !   !   +-------- KbIn1 input
;                              !   +------------ (FontSel0 output)
;                              +---------------- (FontSel1 output)
;
; All regular keys are tied to input line KbIn0. Most modifier keys are tied to
; input KbIn1.  However, some modifier keys are tied to KbIn0.  We need to make
; sure,  that hits on  modifier keys on KbIn0  does not interfere with our scan
; for regular keys.
;------------------------------------------------------------------------------
KEYBOARD_READ_KEY:
	lda #KEY__NONE			; Reset code of key read (= no key
	sta KEY				; read)

	lda #MOD_KEY__NONE		; Reset code of modifier key read (= no
	sta MOD_KEY			; modifier key read)

	lda #0b00000011         	; Port B all inputs that need to be
	ora FONT			; pulled down (be aware: B2 and B3 both
	sta PORT_B			; select the font to be displayed)
		
	ldx #0b00000000			; Select row and column (0, 0)
.READ	
	stx PORT_D			; Output current row/column and keep in
					; register X
	lda PORT_B			; Read key at selected row and column
	and #MASK_KB_IN_0
	beq .KEY_PRESSED
.SKIP_MOD_KEY
	cpx #LAST_KEY_TABLE_ENTRY
	beq .NO_KEY_PRESSED
	inx				; Select next row and column
	cpx #MOD_KEY_COL_ROW_ATARI	; Hits of modifier keys must not stop
	beq .SKIP_MOD_KEY		; scan for regular keys	
	jmp .READ
.NO_KEY_PRESSED
	rts
.KEY_PRESSED
	ldy .SCAN_CODE,x		; Determine scan code and send scan
	bne .READ_MOD_KEYS		; code, but only if scan code is valid.
	rts				; Otherwise KEY will remain KEY__NONE.
					; Furthermore no scan for modifier keys
					; in this case
.READ_MOD_KEYS
	sty KEY				; Remember regular key pressed
.TRY_READ_ATARI:
	ldx #MOD_KEY_COL_ROW_ATARI
	stx PORT_D
	lda PORT_B
	and #MASK_KB_IN_0
	beq .TRY_READ_SHIFT
	lda MOD_KEY
	ora #MOD_KEY__ATARI
	sta MOD_KEY
.TRY_READ_SHIFT:
	ldx #MOD_KEY_COL_ROW_SHIFT
	stx PORT_D
	lda PORT_B
	and #MASK_KB_IN_1
	beq .DONE
	lda MOD_KEY
	ora #MOD_KEY__SHIFT
	sta MOD_KEY
.DONE
	rts

; This table of 64 entries  assigns each row/column output  a corresponding key
; code.  Some rows/columns are not connected to any key of the ATARI 800's key-
; board matrix. KB_IN_0 should never be low in such a case. However, a key code
; of KEY__NONE is assigned in such a case, just to be safe.
; This table  only works  in conjunction with KbIn0.  These are mostly "actual"
; keys, not modifier keys. Modifier keys like SHIFT or CTRL are tied to the in-
; put KbIn1 and are handled separately.
.SCAN_CODE:
	.db 'a', '>', 'q', KEY__NONE, '1', '=', 'z', '*'
	.db 's', '<', 'w', '/', '2', '-', 'x', '+'
	.db 'g', '8', 't', 'm', '5', 'i', 'b', 'k'
	.db KEY__NONE, ASCII__BS, ASCII__TAB, KEY__NONE, ASCII__ESC, ASCII__CR, KEY__NONE, KEY__NONE
	.db KEY__NONE, '7', 'y', 'n', '6', 'u', KEY__NONE, KEY__NONE
	.db 'd', '0', 'e', '.', '3', 'p', 'c', ';'
	.db 'h', KEY__NONE, KEY__NONE, ' ', KEY__NONE, KEY__NONE, KEY__NONE, 'j'
	.db 'f', '9', 'r', ',', '4', 'o', 'v', 'l'
