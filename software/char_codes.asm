;==============================================================================
;
; The VT-6502 Serial Terminal Application
; (c) 2024 by Holger Zahnleiter, all rights reserved
;
; IN THIS FILE:
; Symbolic names for character codes.
;
;==============================================================================

ASCII__BEL = 0x07
ASCII__BS  = 0x08
ASCII__TAB = 0x09
ASCII__LF  = 0x0A
ASCII__CR  = 0x0D
ASCII__ESC = 0x1B
