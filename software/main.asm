;==============================================================================
;
; The VT-6502 Serial Terminal Application
; (c) 2024 by Holger Zahnleiter, all rights reserved
;
; IN THIS FILE:
; Main program  of the VT-6502 serial terminal.  It contains the  main loop and
; includes subroutines from other assembly source files.
;
;==============================================================================

	.cr 6502
	.tf serial-terminal.hex, int

; Program variables start in page zero at this address.  A block of consecutive
; bytes is  required/allocated for  program variables.  But caution is advised!
; The R6501's memory is limited.  The call stack is  also located in page zero.
; It starts at memory location  0x00FF and is growing towards 0x0040.  All pro-
; gram variables will be located from 0x0040 onwards.
	.sm  ram
	.org 0x0040

; This cyclic 16 bit counter  to generate short time intervals  for controlling
; the duration of beeps.  Also used to  flash the LED to  indicate a functional
; system.
;
; The program  actually only checks  the high byte of the counter  as we do not
; need a higher resolution.
BEEP_CNTR:
	.dw 0
BEEP_CNTR_LO     = BEEP_CNTR
BEEP_CNTR_HI     = BEEP_CNTR+1
BEEP_CNTR__LIMIT = 0b0100000

LED_STATE:
	.db 0
LED_STATE__LO  = 0b00000000
LED_STATE__HI  = 0b11111111

; This cyclic 16 bit counter to generate  short time intervals for checking the
; keyboard matrix for key presses.
;
; The program  actually only checks  the high byte of the counter  as we do not
; need a higher resolution
KB_CNTR:
	.dw 0
KB_CNTR_LO     = KB_CNTR
KB_CNTR_HI     = KB_CNTR+1
KB_CNTR__LIMIT = 0b00000100

; A key need to be helt down for a short time before it is automatically repea-
; ted. This variable is used to recognize the first hit and to generate the de-
; lay before the automatically repeated hits.
KEY_REP_CNTR:
	.db 0

; The program resides in ROM, occupying the uppermost 8k of the R6501's address
; space (0xE000 - 0xFFFF).  All program code will be located from 0xE000 onwar-
; ds.
	.sm  code
	.org 0xE000


; Definitions and routines
	.in r6501_mcu.asm
	.in hd6445_crtc.asm
	.in char_codes.asm
	.in display.asm
	.in audio.asm
	.in keyboard.asm
	.in communication.asm


;------------------------------------------------------------------------------
; This is the main loop.  It initalizes the hardware and software.  Then enters
; an infinite loop:
; - Keys pressed are sent, via serial interface, to a host computer.
; - Characters received from the  host computer, via serial interface, are then
;   printed on screen.
;------------------------------------------------------------------------------
MAIN:
	sei			; Disable all interrupts globally

	ldx #STACK_START_ADDR	; Stackpointer points to 0xFF and grows
	txs			; downwards to 0x40

	cld			; Use binary instead of BCD arithmetic

	jsr INIT_HARDWARE
	jsr INIT_SOFTWARE
	jsr INITIAL_BEEP
	jsr CLEAR_SCREEN

	cli			; Allow all interrupts globally now that
				; hardware and software are initialized
.LOOP
	jsr BEEP_IF_REQUIRED
	jsr HANDLE_KEY_PRESSES
	jsr HANDLE_SERIAL_IN
	jmp .LOOP


;------------------------------------------------------------------------------
; Subroutines
;------------------------------------------------------------------------------


;---- Set up R6501 built-in hardware and external video hardware ----
INIT_HARDWARE:
	lda #0b00000000	; Disable all interrupts individually
	sta IER
	
	lda #0b01100000	; Select "normal" bus mode, Port D all pins as outputs
	sta MCR		; (indicator LED), Counter A as interval timer (Baud
			; rate generator), Counter B as interval timer (no
			; beeping at this time)

	jsr INIT_COMMUNICATION_HARDWARE
	jsr INIT_AUDIO_HARDWARE
	jmp INIT_DISPLAY_HARDWARE
	; Note how we jump to INIT_DISPLAY_HARDWARE  instead of calling it as a
	; subroutine.  Instead we jump to INIT_DISPLAY_HARDWARE and  let it re-
	; turn to our caller.  We are using the fact that INIT_DISPLAY_HARDWARE
	; also ends with an rts opcode.


;--- Set up program variables (in page zero) to initial values ----
INIT_SOFTWARE:
	lda #0
	sta BEEP_CNTR_LO
	sta BEEP_CNTR_HI
	sta KB_CNTR_LO
	sta KB_CNTR_HI
	sta KEY_REP_CNTR

	lda #LED_STATE__LO
	sta LED_STATE

	jsr INIT_COMMUNICATION_SOFTWARE
	
	jsr INIT_KEYBOARD_SOFTWARE
	
	jmp INIT_DISPLAY_SOFTWARE
	;;; rts - No rts required, INIT_DISPLAY_SOFTWARE will rts for us


;---- Starts/stops beeps and toggles LED ----
; Runs a cyclic counter  to achive a certain  time interval.  On timer overflow
; the LED is toggled so that we get an  indication whether the system is opera-
; tional.
; Also the speaker gets silenced in this case. Beeps are started elswere in the
; code (fire and forget). By stopping them after this defined interval we achi-
; eve short beeps of the same length.
BEEP_IF_REQUIRED:
	lda BEEP_CNTR_HI	; Intervall done?
	cmp #BEEP_CNTR__LIMIT
	bcc .INC_CNTR		; NO

	lda #0			; YES, reset counter
	sta BEEP_CNTR_LO	
	sta BEEP_CNTR_HI

	jsr STOP_BEEPING	; Will have effect only in case speaker was
				; actually beeping
	jmp TOGGLE_LED
	; TOGGLE_LED does rts for us
.INC_CNTR
	clc
	lda BEEP_CNTR_LO
	adc #1
	sta BEEP_CNTR_LO
	bcc .DONE
	inc BEEP_CNTR_HI
.DONE
	rts


;---- Toggles LED ----
TOGGLE_LED:
	lda LED_STATE		; Set LED (port) to current value (either 0
	sta PORT_C		; or 1)
	
	eor #LED_STATE__HI	; Toggle current value for next time
	sta LED_STATE

	rts


;---- Handles key presses ----
HANDLE_KEY_PRESSES:
	lda KB_CNTR_HI		; Intervall done?
	cmp #KB_CNTR__LIMIT
	bcc .INC_CNTR		; NO

	lda #0			; YES, reset counter
	sta KB_CNTR_LO	
	sta KB_CNTR_HI

	jsr KEYBOARD_READ_KEY	; Read keyboard an handle keys, if keys pressed
	lda KEY			; KEY = KEY__NONE = 0, if no key pressed
	beq .NO_KEY_PRESSED

	lda KEY_REP_CNTR
	cmp #0
	beq .FIRST_PRESSED	; First pressed, handle key immediately
	cmp #7			; Counter limit reached? A ≥ 4?
	bcs .TRY_ATARI_KEY	; YES, generate repeated presses
	inc KEY_REP_CNTR	; NO, further counting key helt down
	jmp .INC_CNTR
.FIRST_PRESSED:
	lda #1
	sta KEY_REP_CNTR
.TRY_ATARI_KEY:
	lda MOD_KEY
	and #MOD_KEY__ATARI
	bne .TRY_SHIFT_KEY
	jsr HANDLE_ATARI_KEY
	jmp .INC_CNTR
.TRY_SHIFT_KEY:
	lda MOD_KEY
	and #MOD_KEY__SHIFT
	bne .TRY_CONTROL_KEY
	jsr HANDLE_SHIFT_KEY
	jsr SERIAL_SEND_CHAR	; Send char in register Y
	jmp .INC_CNTR

.TRY_CONTROL_KEY:
	lda MOD_KEY
	and #MOD_KEY__CTRL
	bne .NO_MOD_KEY_PRESSED
	jsr HANDLE_CTRL_KEY
	jmp .INC_CNTR

.NO_MOD_KEY_PRESSED	
	ldy KEY			; Send key to host computer via serial inter-
	jsr SERIAL_SEND_CHAR	; face, key code expected in register Y
	jmp .INC_CNTR
.NO_KEY_PRESSED
	lda #0			; Reset mechanism for generating automatically
	sta KEY_REP_CNTR	; repeated key presses (just in case)
.INC_CNTR
	clc
	lda KB_CNTR_LO
	adc #1
	sta KB_CNTR_LO
	bcc .DONE
	inc KB_CNTR_HI
.DONE
	rts


;--- Pick a font, depending on number keys ----
; Assumes the Atari key has been pressed.  Activates one out of  four fonts de-
; pending on keys '1', '2', '3', or '4' have been pressed too.
HANDLE_ATARI_KEY:
	lda KEY
.TRY_FONT_SERIF
	cmp #'1'
	bne .TRY_FONT_SANS_SERIF
	ldy #FONT__SERIF
	jmp PICK_FONT_Y
.TRY_FONT_SANS_SERIF
	cmp #'2'
	bne .TRY_FONT_COMPUTER
	ldy #FONT__SANS_SERIF
	jmp PICK_FONT_Y
.TRY_FONT_COMPUTER
	cmp #'3'
	bne .TRY_FONT_TEMPLATE
	ldy #FONT__COMPUTER
	jmp PICK_FONT_Y
.TRY_FONT_TEMPLATE
	cmp #'4'
	bne .NO_FONT_PICKED
	ldy #FONT__TEMPLATE
	jmp PICK_FONT_Y
.NO_FONT_PICKED
	rts


;--- Pick alternate char for key + SHIFT ----
; Shifted key code will be returned in register Y.
HANDLE_SHIFT_KEY:
	lda KEY			; 'a' ≤ KEY ≤ 'z'
	cmp #'a'
	bcc .NOT_ALPHA
	cmp #'z'+1
	bcs .NOT_ALPHA
.ALPHA
	and #0b11011111		; 'a' --> 'A'
	tay
	rts
.NOT_ALPHA			; Not the simple case of letters. Scan the 
	ldx #0			; mapping table instead. If the key code in A
.NEXT_CHAR			; a key code in the table, then return the
	cmp .KEY_CODE,x		; corresponding shifted key kode from the
	beq .HIT		; mapping table in Y
	cpx #17
	beq .NO_MAPPING
	inx
	jmp .NEXT_CHAR
.HIT
	lda .SHIFTED_KEY_CODE,x
.NO_MAPPING
	tay			; Return shifted or unshifted key code alike in		
	rts			; register Y
.KEY_CODE
	.db '*', '+', ',', '-', '.', '/', ';', '=', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
.SHIFTED_KEY_CODE
	.db '^', '\\', '[', '_', ']', '?', ':', '|', ')', '!', '"', '#', '$', '%', '&', '\'', '@', '('


;--- For example: CTRL+G = BEL ----
HANDLE_CTRL_KEY:
	lda KEY
.TRY_FONT_SERIF
	cmp #'g'
	bne .NO_KEY
	ldy #ASCII__BEL
	jsr SERIAL_SEND_CHAR	; Send char in register Y
.NO_KEY
	rts


;---- Handle bytes received, if any. ----
HANDLE_SERIAL_IN:
	jsr INPUT_BUFFER_GET	; Try take byte from receive ring buffer
	cpx #INPUT_BUFFER_GET__FOUND
	bne .NOTHING_RECEIVED
	
	jsr PRINT_BYTE		; If byte available, then print on screen

	jmp HANDLE_SERIAL_IN	; Try to deplete the receive buffer quickly

.NOTHING_RECEIVED		; Check if there were input buffer overflows
	lda INPUT_BUFFER_PUT_STATUS	
	cmp #INPUT_BUFFER_PUT_STATUS__OVERFLOW
	bne .DONE		; NO, no overflow
	
	; CLEAR_INPUT_BUFFER  is an alias for  INIT_COMMUNICATION_SOFTWARE  but
	; easier to understand here in this context.
	jsr CLEAR_INPUT_BUFFER	; YES, an overflow
	jsr START_ERR_BEEP
.DONE
	rts


;------------------------------------------------------------------------------
; Interrupt service routines
;------------------------------------------------------------------------------

NMI_ISR:
	rti

IRQ_BRK_ISR:
	sei			; Suppress all interrupts
	pha			; Save register A
	tya			; Save register Y
	pha
	
	lda IER			; Check which interrupt source (out of 8
	and #IER__RCVR_FLAG	; possible sources)
	beq .DONE
.HANDLE_SERIAL_RECEIVE
	lda SCDAT		; Read byte received (also clears SCSR__RDRF).
	jsr INPUT_BUFFER_PUT	; Main routine should check
				; INPUT_BUFFER_PUT_STATUS
.DONE
	pla			; Restore register Y
	tay
	pla			; Restore register A
	cli			; Permit all interrupts
	rti


;------------------------------------------------------------------------------
; Hardware vectors
;------------------------------------------------------------------------------
		
	.org 0xfffa
	.dw NMI_ISR	; NMI vector
	.dw MAIN	; RESET vector
	.dw IRQ_BRK_ISR	; IRQ and BRK vector

	.end
