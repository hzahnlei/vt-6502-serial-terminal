###############################################################################
#
# VT-6502 Serial Terminal
#
# Build image based on Alpine Linux including the SB-Assember V3.
# SB-Assember see https://github.com/sbprojects/sbasm3
#
# (c) 2024 by Holger Zahnleiter, all rights reserved
#
###############################################################################

ARG BASE_IMAGE_TAG

FROM alpine:$BASE_IMAGE_TAG

ARG BASE_IMAGE_TAG
ARG CUSTOM_IMAGE_TAG

LABEL maintainer="Holger Zahnleiter <opensource@holger.zahnleiter.org>" \
	org.label-schema.vendor="Holger Zahnleiter" \
	org.label-schema.name="Alpine Linux image for building 8 bit assembly programs with SB-Assembler" \
	org.label-schema.license="MIT" \
	org.label-schema.build-date=$BUILD_DATE \
	org.label-schema.version="$BASE_IMAGE_TAG:$CUSTOM_IMAGE_TAG" \
	org.label-schema.schema-version="1.0.0-rc.1" \
	org.label-schema.vcs-ref=$VCS_REF \
	org.label-schema.vcs-type="git" \
	org.label-schema.vcs-url="https://gitlab.com/hzahnlei/vt-6502-serial-terminal"

RUN echo http://nl.alpinelinux.org/alpine/edge/main >> /etc/apk/repositories \
	echo http://nl.alpinelinux.org/alpine/edge/community >> /etc/apk/repositories \
	echo http://nl.alpinelinux.org/alpine/edge/testing >> /etc/apk/repositories

RUN apk add --update --no-cache \
	make \
	git \
	python3

RUN git clone https://github.com/sbprojects/sbasm3.git

ENV PATH="/sbasm3/:$PATH"
