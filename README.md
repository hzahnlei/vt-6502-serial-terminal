# VT-6502 Serial Terminal

![dt-1](media/dt-1_teaser_preliminary.png "DT-1")

This project is about building a serial terminal in 2023.
It uses:

-  A real tube (CRT) from an old Brother WP-1 word processor as its display
-  A 6502 MCU for its brain
-  A vintage keyboard from an old Atari 800 computer for user input

Everything is housed in a 3D printed case that is supposed to convey a vintage look and feel.

This repository contains the Fusion 360 CAD files as well as STL and STEP exports.
Furthermore the source code is included as well as schematics and assembly instructions.

Have fun!
2023-06-06, Holger Zahnleiter

## Documentation

- Electronics
  - [Analyzing WP-1 Circuitry](notes/README.md)
  - [Prototypical Text Video Display](electronics/video_prototype/README.md)
  - MPU Board TODO
- Mechanics
  - TODO
- Software
  - [Analyzing WP-1 ROMs](rom_dump/README.md)
  - TODO

## Some Remarks on The Brother WP-1

In fact, back in the days I considered monochrome CRTs dull.
The were used in boring business computers.
Our home computers had colors, sprites and sound and required a much cooler color CRT.
Later I was very happy to get rid of CRTs and fully embraced TFT displays.

But times change.
For quite some time now I am fascinated by old cathode ray tube (CRT) displays.
Especially the monochrome ones.
In my head the idea of building something with a CRT took form.
So I could not resist to by one, when I discovered an old Brother WP-1 on Ebay.
One year later I could even purchase another one by auction.
Both were surprisingly cheap.

Both devices functioned right away.
And both are displaying a very sharp and stable image on screen.
I cannot say this about all my old computers.
CRT images are often bent or flickering/collapsing due to a degraded/instable electronic components and/or power supplies.
It seems that Brother really designed and built a high quality product back in the day.

## Disclaimer

This is a free, non commercial and open source project.
You are using this on your own risk.
I am not responsible nor liable for any damage, accidents or injuries it may cause.

## Credits

This project is only possible because of the availability of free (sometimes also open source) software and services:

* The CAD design has been done using the free version of Autodesk's [Fusion 360](https://www.autodesk.de/products/fusion-360/)
* This Git repository is hosted on [GitLab](https://gitlab.com)
* I am using [Cura](https://ultimaker.com/de/software/ultimaker-cura) for slizing the 3D models for printing
* I was using z80dasm for disassembling the WP-1 ROMS: https://www.tablix.org/~avian/blog/articles/z80dasm/
* Another interesting tool is the z80asm assembler: http://savannah.nongnu.org/git/?group=z80asm
* I have learned some WP-1 details from David Given's projects
  * https://github.com/davidgiven/bterm
  * https://github.com/davidgiven/cpmish
