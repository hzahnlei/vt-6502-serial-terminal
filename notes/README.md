# Notes

## Brother WP-1

It is hard to find detailed information on the Brother WP-1 after decades.
Here are some sources:

* **David Given**
  * Turing a WP-1 into a serial Terminal - https://github.com/davidgiven/bterm
  * Porting CP/M to an WP-1 - https://github.com/davidgiven/cpmish/tree/wp1
* **Brother WP-2450DS Service Manual**
  Sadly I could only find a manual for the WP-2450DS.
  This machine seems to be closely related.
  However, there are differences.
  For example: The WP-2450DS manual lists a gate array that does the job of an CRTC.
  My old WP-1 on the other side houses an actual, discrete HD6445 CRTC-II.
  As a result one can definitely leran relevant facts from the manual,
  while other information need to be found by analysis of hardware and code.
* More helpful is an old application note [AN-0851](../datasheets/AN-0851:%20Motorola%20MC6845%20CRTC%20Simplifies%20Video%20Display%20Controllers.pdf) from Motorola.
  It targets the 6845 but the HD6445 is a sibling and close enough so that insights from AN-0851 can be applied to it.

Most interesting for this project:
* **From the manual**
  * CRT Dimensions: 5" × 9" (12.7cm × 22,86cm)
  * Character resolution: 91 columns × 15 lines
  * Pixel resolution: 819 × 240
  * Stride: 192
  * Pixel clock frequency: 15.33MHz
  * Character size: 9 × 16 pixel
* **From David Given's work**
  * The HD6445 CRTC-II seems to be mapped to I/O addresses 0x80 and 0x81.
  * Video RAM seems to be mapped to RAM starting at 0xE000.

Excerpt from David Given's work:

```
VIDEORAM_BASE = 0xE000
VIDEO_ADDR    = 0x80
VIDEO_DATA    = 0x81
```

David Given's programs are booted under control of the WP-1 hardware.
Therefore the CRTC is already initialized.
So we cannot learn this from David Given's code but only from the WP-1 ROMs.

### Clock Frequency for Video Circuit

The clock frequency is 15.33MHz according to the service manual.
The value printed on the crystal is consistent with this.
The crystal is labeled "XTAL2" on the Brother WP-1 PCB.

It provides the pixel clock.
It is connected to an gate array.
I assume the character clock for the CRTC is derived by the gate array from the pixel clock.
1. When you take a look at the the service manual you will find that it displays 91×15 characters.
   Each character is 9×16 pixels.
   Assuming a frame rate of 60Hz this makes 91*15*9*16*60=11,793,600 pixels per seconds (ca. 11MHz).
   This is much less that the 15.33MHz clock.
1. But we must not forget that in reality the beam needs additional time for horizontal and vertical overscan and retrace.
   If you now take a look at the initialization routine from the WP-1 ROMs you will see that it is configured to 117×15 characters.
   Now, if we calculate again this makes 117*15*9*16*60=15,163,200, that is already close.
1. Finally the initialization code tells us that there are 4 additional raster lines adjustment.
   This should bring us even closer to the 15.33MHz. 

![crtc crystal closeup](../media/crtc_crystal_closeup.png "CRTC Crystal Closeup"){width=25%}

### Power Supply for The CRT

From the service manual I get it that the CRT driver PCB is supplied with 12V.
I measured a current drain of about 770mA.

![crt driver pcb current drain](../media/crt_driver_current_drain.png "CRT Driver PCB Current Drain"){width=25%}

### Original Video Clock Circuit

From the PCB I traced the following video clock circuitry.
This however only covers the generation of the pixel clock (aka dot clock) as the character clock gets derived inside a gate array.

![original video clock circuit](../media/original_video_clock.png "Original Video Clock Circuit"){width=25%}

For my hand-soldered prototyping board I am using my own modified clock generation circuit.
I started from an old Motorola application note ([AN-0851](../datasheets/AN-0851:%20Motorola%20MC6845%20CRTC%20Simplifies%20Video%20Display%20Controllers.pdf)) and modified it to my needs.
This circuit not only generates the pixel clock but also the character clock.

![my video clock circuit](../media/my_video_clock.png "My Video Clock Circuit"){width=50%}

The final board version therefore uses a slightly different wiring of the 74LS163 counter.
See the schematic for the final PCB.

### Original vs. Reworked Shift Register

During prototyping phase I recognized that the 74LS165 shift register introduced a problem:
- The 74LS165 shift register is transparent in that the most significant bit of the pixel pattern gets immediately visible at the output when loading the bit pattern into the shift register.
- The load occurs at the same time of the first shift clock. The 74LS165 ignores that shift clock when loading.
  Therefore, the generated picture was missing the rightmost pixel.

The final board version therefore uses a 74LS166 shift register.
This register is also transparent.
The difference is that the shift clock occuring with the load impulse already shifts the pattern.
So, all 8 pixels are getting pushed out to the tube screen as expected.

### Original Video Signal Output

From the PCB I traced the following video output circuitry.
However, I cannot read the values for the coils.
They are not printed on the parts nor to be found in the service manual.
At least I could read the resistor values.

![original video output circuit](../media/original_video_out.png "Original Video Output Circuit"){width=50%}

## Wiring the New PSU and Appliance Switch

The power switch ([data sheet](../datasheets/marquardt_appliance_switch.pdf)) needs to be wired properly.
Otherwise it will light up regardless of switched on or off.
Here is the wiring diagram:

![appliance switch wiring diagram](../media/appliance_switch_wiring_diagram.png "Appliance Switch Wiring Diagram"){width=25%}

## Z80 Reset Behaviour

Upon reset, the Z80 MPU does the following:

* IFF1 and IFF2 are cleared.
* The interrupt mode is set to 0 (mode 0).
* The PC is set to zero, that is the MPU will start executing code at 0x0000.
  * Trap interrupts will also execute from 0x0000.
    In the ISR one needs to first check the `TRAP` flag in the `ITC` register to determin whether reset or trap.
* I and R registers are cleared.
* SP is set to 0xffff.
  That is, the stack will grow downwards from 0xffff to 0x0000.
* A and F registers are set to 0xff.

## Atari 800 Keyboard

The following diagram shows how the keyboard is connected to the Atari 800 mainboard.
The resistors used (R110 - R126) are 100Ω, according to the field and service manual.
The capacitors seem to be 0.001µF (25V) ceramic.
The scan has been taken from Atari's "Atari 400/800 Hardware Manual", page 88.

![atari 800 keyboard circuit](../media/atari800_kb_circuit.png "Atari 800 Keyboard Circuit"){width=50%}

This is the Atari 800 keyboard matrix.
The scan has been taken from Atari's "Atari 400/800 Field Service Manual", page 7-38.

![atari 800 keyboard matrix](../media/atari800_kb_matrix.png "Atari 800 Keyboard Matrix"){width=50%}
