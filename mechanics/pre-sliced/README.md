# About the Pre-Sliced Files

The files herein are pre-sliced for the Creality CR-10 Max.
The parts are relatively big as the CR-10 Max features a relatively huge build volume.
Cutting the original model in smaller pieces might be required, if you are using a smaller printer.

The slicer used is Cura 5.1.0.

Most pieces are sliced for a 0.8mm nozzle.
Otherwise printig such lage pieces would take too long.
Such files end with an `0.8mm` suffix.
Some pieces have ben sliced for a 0.4mm nozzle though.
These files end with an `0.4mm` suffix.

All parts have been printed with PLA as this is easy to print.

To achieve a premium surface, a lot of filling and sanding is required before spray painting the case.
