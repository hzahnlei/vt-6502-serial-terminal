###############################################################################
#
# VT-6502 Serial Terminal
#
# - Building a Linux Docker image that includes SB-Assembler,  so that 6502 as-
#   sembly programs can be build in an automaticated CI enviroment.
# - Building several test programs as well as the actual serial terminal appli-
#   cation.
#
# (c) 2024 by Holger Zahnleiter, all rights reserved
#
###############################################################################

.PHONY: firmware image push

.EXPORT_ALL_VARIABLES:
REGISTRY := registry.gitlab.com/$(or $(CI_PROJECT_NAMESPACE),hzahnlei)/$(or $(CI_PROJECT_NAME),vt-6502-serial-terminal)
GITLAB_URL := https://gitlab.com/$(or $(CI_PROJECT_NAMESPACE),hzahnlei)/$(or $(CI_PROJECT_NAME),vt-6502-serial-terminal)
BASE_IMAGE_TAG := 3.19.1
CUSTOM_IMAGE_NAME := sbasm
CUSTOM_IMAGE_TAG := 1.0.0
CUSTOM_IMAGE_FULL_NAME := $(REGISTRY)/$(CUSTOM_IMAGE_NAME):$(CUSTOM_IMAGE_TAG)

# Assembles the  terminal application locally.  Requires SB-Assembler (and thus
# Python 3)  to be installed locally.  (But does  not require a  Docker instal-
# lation.)
firmware:
	cd software && make all

# Copy firmware  to my only Windows computer.  Windows is required by  my EPROM
# programming device. No Linux nor macOS drivers and utility available.
# This only works on macOS, if holger's home directory on Windows is shared and
# the Macintosh is connected to that Windows machine.
copy:
	cp -f software/serial-terminal.hex /Volumes/Users/holger/Desktop

# Assembles the terminal application in a container.  Requires no local instal-
# lation of  SB-Assembler (nor Python 3).  Docker hast to be  installed locally
# however. The assembled hex file will be written into the mounted host folder.
# Therefore, the result of assembly will be available on the host.
firmware-dockerized:
	docker run \
	       --volume  ./software:/software \
	       --workdir /software \
	       $(CUSTOM_IMAGE_FULL_NAME) \
	       'make' 'all'


# Assembles the testprograms locally. Requires SB-Assembler (and thus Python 3)
# to be installed locally. (But does not require a Docker installation.)
testprograms:
	cd electronics/mpu_prototype/test_programs && make all

# Assembles the test programs in a container. Requires no local installation of
# SB-Assembler (nor Python 3). Docker hast to be installed locally however. The
# assembled hex files will be written into the mounted host folder.  Therefore,
# the result of assembly will be available on the host.
testprograms-dockerized:
	docker run \
	       --volume  ./electronics/mpu_prototype/test_programs:/test_programs \
	       --workdir /test_programs \
	       $(CUSTOM_IMAGE_FULL_NAME) \
	       'make' 'all'

# Opens an interactive Docker session.  The project is mounted into the contai-
# ner.  SB-Assembler and Python 3  are installed  inside the container.  So the
# programs can be built inside the container without the need for local instal-
# lation.  The assembled hex files, or any change to the files for that matter,
# will be written into the mounted host folder. Therefore, the result of assem-
# bly or editing will be available on the host.
# You may even  remote-connect into this container  with Visual Studio Code for
# development, whithout having to install too many packages on your host machi-
# ne.
dockerized:
	docker run \
	       --interactive \
	       --tty \
	       --volume  .:/vt-6502 \
	       --workdir /vt-6502 \
	       $(CUSTOM_IMAGE_FULL_NAME)

# Building the Docker image can be done manually  on the local machine and from
# within automated CI pipelines.
image:
	docker build -f Dockerfile \
	             --build-arg BASE_IMAGE_TAG \
	             --build-arg CUSTOM_IMAGE_TAG \
	             -t $(CUSTOM_IMAGE_FULL_NAME) .
	
# This however  only works from within  an automated CI pipeline.  It will push
# the Docker image into the CI provider's Docker image registry.
push: image
	echo "${CI_REGISTRY_PASSWORD}" | docker login --username "${CI_REGISTRY_USER}" --password-stdin registry.gitlab.com
	docker push $(CUSTOM_IMAGE_FULL_NAME)
