# Brother WP-1 ROM Dump and Analysis

## Dumping The ROMs

The files contained herein are dumps of the WP-1 ROMs found on the motherboard.
Both chips are Mitsubishi M5L27512K.
Each providing 64kB of space.
Both ROMs have been dumped in binary (`.bin`) and Intel hex format (`.hex`).

*  The first ROM is labeled "U78239".
   Its location on the BCB it labeled "#B1".
   The dump reveals something like a reset and interrupt vector table right at the beginning.
   So this seems to be the startup ROM.
*  The second ROM is labeled "U78241".
   Its location on the BCB it labeled "#B2".

![rom chips closeup](../media/original_rom_chips.png "ROM Chips Closeup"){width=25%}

![dumping roms](../media/dumping_original_roms.png "Dumping ROMs"){width=50%}

## ROM U78239 Analysis

The very beginning of the ROM dump shows a distinctive feature.
A quite regular layout that looks like a reset and interrupt vector table.
The Z80/Z180/64180 expects eight small routines equally spaced to the first 64 bytes of memory.
These are supposed to handle reset/trap (0x0000) and software interrupts (`RST`).
They are followed by some more bytes to form a jump table for external/internal interrupts and the routine for handling NMI.
And indeed, the Z80 starts executing code at 0x0000 after reset.

```
0000  C3 6B 00 00 00 00 00 00  C3 10 04 00 00 00 00 00
0010  C3 25 04 00 00 00 00 00  C3 5E 04 00 00 00 00 00
0020  C3 FD 0A 00 00 00 00 00  C3 26 0B 00 00 00 00 00
0030  C3 6D 0B 00 00 00 00 00  C3 38 00 00 00 00 00 00
0040  3C 02 42 02 E4 02 EA 02  68 00 68 00 F0 02 68 00
0050  68 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00
0060  00 00 00 00 00 00 ED 45  C3 68 00
```

Let's see if a (manual) translation to assembly helps us recognizing the rational behind all this.

```
;---- RST 00/Reset/Trap -------------------------------------------------------
0000  C3 6B 00  JP   006B      ;Expect initialization at 0x006b.
0003  00        NOP
...
;---- RST 08 ------------------------------------------------------------------
0008  C3 10 04  JP   0410
000B  00        NOP
...
;---- RST 10 ------------------------------------------------------------------
0010  C3 25 04  JP   0425
0013  00        NOP
...
;---- RST 18 ------------------------------------------------------------------
0018  C3 5E 04  JP   045E
001B  00        NOP
...
;---- RST 20 ------------------------------------------------------------------
0020  C3 FD 0A  JP   0AFD
0023  00        NOP
...
;---- RST 28 ------------------------------------------------------------------
0028  C3 26 0B  JP   0B26
002B  00        NOP
...
;---- RST 30 ------------------------------------------------------------------
0030  C3 6D 0B  JP   0B6D
0033  00        NOP
...
;---- RST 38/INT0 -------------------------------------------------------------
0038  C3 38 00  JP   0x0038    ;An infinite loop?
003B  00        NOP
...
;---- Interrupt verctors ------------------------------------------------------
0040  3C 02                    ;INT1  --> 023c
0042  42 02                    ;INT2  --> 0242
0044  E4 02                    ;PRT0  --> 02E4
0046  EA 02                    ;PRT1  --> 02EA
0048  68 00                    ;DMA0  --> 0068 (infinite loop, unused?)
004A  68 00                    ;DMA1  --> 0068 (infinite loop, unused?)
004C  F0 02                    ;CSI/O --> 02F0
004E  68 00                    ;ASCI0 --> 0068 (infinite loop, unused?)
0050  68 00                    ;ASCI1 --> 0068 (infinite loop, unused?)
0052  00        NOP
...
;---- NMI ---------------------------------------------------------------------
0066  ED 45     RETN           ;Return immediately, NMI effectively unused.
0068  C3 68 00  JP 0068        ;Infinite loop.
```

So much for the reset/trap, `RST`, NMI, INT0 and other interrupts.
Now for the actual reset routine (which is also triggered on trap) and should originate at 0x006B.

It will take quite some time before we identify the code to initialize the CRTC, if we continue (manually) this way.
Therefore I used a disassembler ([z80dasm](https://www.tablix.org/~avian/blog/articles/z80dasm/)) looking for some `OUT0 (80),r` or so.
And, indeed, I found this routine:

```
l24a5h:
	ld hl,0f4c7h
	ret
sub_24a9h:
	di
	outo (0x80), d
	nop
	outo (0x81), a
	ei
	ret
```

The `RET` right in front of the routine indicates that the code located in front of it is actually completed and a new unit of code starts at label `sub_24a9h`.
The routine sets CRTC register denoted by CPU register `D` to the value denoted by CPU register `A`.
The CRTC's register select (or address) register is mapped to the HD64180's I/O location 0x80.
Data is written to the selected CRTC register by a write to I/O location 0x81.

So, this routine is used to set the registers of the HD6445 CRTC-II.
Now we have to find places in the code where this routine gets called.
We need to figure out the values of CPU registers `D` and `A` and compare it withe the CRTC's datasheet to understand how the CRTC is configured on a more abstract level.

This seems to be a routine that is central to initialization.
It writes CRTC registers in bulk with values from a table:

```
	ld d,000h         ;CRTC register index in D. 
	ld ix,l263dh      ;Values to be found at 0x263d, loaded into A.
l2466h:
	ld a,(ix+000h)
	call sub_24a9h    ;Set CRTC register denoted by D to value in A.
	inc ix            ;Next init value.
	inc d             ;Next CRTC regster.
	ld a,010h         ;Skip CRTC registers R16 and R17, no lightpen.
	cp d
	jr z,l247fh
	ld a,01ch         ;Skip CRTC register R28, no lightpen.
	cp d
	jr z,l2483h
	ld a,028h         ;Done after 40 (0-39) CRTC registers
	cp d
	jr nz,l2466h
	ret
l247fh:
	ld d,012h         ;Continue with CRTC register R18.
	jr l2466h
l2483h:
	ld d,01dh         ;Continue with CRTC register R29.
	jr l2466h
```

Next comes the accompanying table.
Be advised that it contains fewer values than there are registers.
This is because the code above skips CRTC registers R16, R17, and R28.

```
2630                                          75 5B 5D
2640  FF 0F 04 0F 0F 50 0F 70  00 00 00 00 00 01 04 C0
2650  00 00 C0 0C 01 80 00 00  2B 00 48 65 20 00 00 00
2660  00 00
```

Code and table should give us the following HD6445 initialization:

| Register | Name                           | Initial value     | Unit      | Flags                             | Description |
| -------- | ------------------------------ | ----------------- | --------- | --------------------------------- | ----------- |
| R0       | Horizontal total characters    | 0x75 (117)        | Character | n/a                               |  |  
| R1       | Horizontal dispayed characters | 0x5B (91)         | Character | n/a                               | **Display 91×15 characters** |  
| R2       | Horizontal sync position       | 0x5D (93)         | Character | n/a                               |  |  
| R3       | Sync width                     | 0xFF (15/15)      | See #1    | `WV3¦WV2¦WV1¦WV0¦WH3¦WH2¦WH1¦WH0` | WV=0xF: Vertical sync for 15 raster lines<br><br>WH=0xF: Horizontal sync for 15 characters |
| R4       | Vertical total rows            | 0x0F (15)         | Row       | n/a                               |  |
| R5       | Vertical total adjust          | 0x04              | Raster    | n/a                               |  |
| R6       | Vertical displayed rows        | 0x0F (15)         | Row       | n/a                               |  |
| R7       | Vertical sync position         | 0x0F (15)         | Row       | n/a                               |  |
| R8       | Interlace mode and skew        | 0x50 (0b01010000) | n/a       | `C1 ¦C0 ¦D1 ¦D0 ¦0  ¦0  ¦V  ¦S  ` | C1=1, C0=0: 2 characters skew<br><br>D1=1, D0=0: 2 characters skew<br><br>S=0: **non-interlaced** and V irrelevant |
| R9       | Max raster address             | 0x0F (15)         | Raster    | n/a                               |  |
| R10      | Cursor 1 start                 | 0x70 (0b01110000) | Raster    | `0  ¦B1 ¦P1 ¦   ¦   ¦   ¦   ¦   ` | B1=1, P1=1: **Cursor blinking** slowly (1/32)<br><br>Starts at raster line 0b10000=16 |
| R11      | Cursor 1 end                   | 0x00              | Raser     | n/a                               |  |
| R12      | Screen 1 start address (high)  | 0x00              | Mem addr  | n/a                               | Screen 1 at 0x0000 |
| R13      | Screen 1 start address (low)   | 0x00              | Mem addr  | n/a                               |  |
| R14      | Cursor 1 address (high)        | 0x00              | Mem addr  | n/a                               | Cursor 1 at 0x0000 |
| R15      | Cursor 1 address (low)         | 0x00              | Mem addr  | n/a                               |  |
| R16      | Light pen (high)               | Skipped           | n/a       | n/a                               | No light pen |
| R17      | Light pen (low)                | Skipped           | n/a       | n/a                               | No light pen |
| R18      | Screen 2 start position        | 0x01 (1)          | Row       | n/a                               |  |
| R19      | Screen 2 start address (high)  | 0x04              | Mem addr  | n/a                               | Screen 2 at 0x04C0. |
| R20      | Screen 2 start address (low)   | 0xC0              | Mem addr  | n/a                               |  |
| R21      | Screen 3 start position        | 0x00 (0)          | Row       | n/a                               |  |
| R22      | Screen 3 start address (high)  | 0x00              | Mem addr  | n/a                               | Screen 3 at 0x00C0. |
| R23      | Screen 3 start address (low)   | 0xC0              | Mem addr  | n/a                               |  |
| R24      | Screen 4 start position        | 0x0C (12)         | Row       | n/a                               |  |
| R25      | Screen 4 start address (high)  | 0x01              | Mem addr  | n/a                               | Screen 4 at 0x0180. |
| R26      | Screen 4 start address (low)   | 0x80              | Mem addr  | n/a                               |  |
| R27      | Vertical sync position adjust  | 0x00              | Raster    | n/a                               |  |
| R28      | Light pen raster               | Skipped           | n/a       | `DP ¦0  ¦0  ¦   ¦   ¦   ¦   ¦   ` | No light pen, read only |
| R29      | Smooth scrolling               | 0x00              | Raster    | n/a                               |  |
| R30      | Control 1                      | 0x2B (0b00101011) | n/a       | `VE ¦VS ¦IB ¦IL ¦SY ¦TV ¦SP1¦SP0` | VE=0, VS=0, TV=0: Master CRTC producing HSYNC and VSYNC and active DISPTMG<br><br>IB=1, IL=0: Interrupt on vertical retrace<br><br>SY=1: fine adjust (see R27)<br><br>SP1=1, SP0=1: use all 4 screens |
| R31      | Control 2                      | 0x00 (0b00000000) | n/a       | `SS4¦SS3¦SS2¦SS1¦RI ¦0  ¦0  ¦0  ` | SS=0b: All screens hard scrolling. R29 effectively irrelevant.<br><br>RI=0: normal scan, text in **normal height**.<br><br>**Returns status, when read.** |
| R32      | Control 3                      | 0x48 (0b01001000) | n/a       | `CM ¦C2 ¦CW1¦CW2¦MW ¦TC ¦DR ¦0  ` | CM=0: XOr cursors when overlapping<br><br>C2=1: **use cursor 2** in addition to cursor 1<br><br>CW1=0: ignore cursor width in R38<br><br>CW2=0: ignore cursor width in R39<br><br>MW=1: Scrolling by "memory width"<br><br>TC=0: Three state mode disabled (cannot use TSC pin)<br><br>DR=0: MPRAM mode not used |
| R33      | Memory width offset            | 0x65 (101)        | Character | n/a                               |  |
| R34      | Cursor 2 start                 | 0x20              | Raster    | n/a                               |  |
| R35      | Cursor 2 end                   | 0x00              | Raster    | n/a                               |  |
| R36      | Cursor 2 address (high)        | 0x00              | Mem addr  | n/a                               | Cursor 2 at 0x0000 |
| R37      | Cursor 2 address (low)         | 0x00              | Mem addr  | n/a                               |  |
| R38      | Cursor 1 width                 | 0x00              | Char      | n/a                               | Custom cursor 1 width is ignored (see CW1 in R32/Control 3) |
| R39      | Cursor 2 width                 | 0x00              | Char      | n/a                               | Custom Cursor 2 width is ignored (see CW2 in R32/Control 3) |

Note #1: Vertical unit are rasterlines, horizontal units are characters.
