;; ----------------------------------------------------------------------------
;; General definitions
;; ----------------------------------------------------------------------------

(def! copy_printable_ascii_glyphs! (fun [src_font dest_font]
                                        (map' (range FIRST_PRINTABLE_ASCII LAST_PRINTABLE_ASCII)
                                              (fun glyph (copy_glyph! from: src_font,glyph
                                                                      to:   dest_font,glyph )) ) ))

(def! copy_umlauts! (fun [src_font dest_font]
                         (do (copy_glyph! src_font,CP850_ä  dest_font,1)
			     (copy_glyph! src_font,CP850_Ä  dest_font,2)
			     (copy_glyph! src_font,CP850_ö  dest_font,3)
			     (copy_glyph! src_font,CP850_Ö  dest_font,4)
			     (copy_glyph! src_font,CP850_ü  dest_font,5)
			     (copy_glyph! src_font,CP850_Ö  dest_font,6)
			     (copy_glyph! src_font,CP850_sz dest_font,7) ) ))

(def! copy_special_glyphs! (fun [misc_font dest_font]
                                (copy_glyph! misc_font,0  dest_font,0) ))

(def! duplicate_ascii_glyphs_to_upper_half!
		(fun font (map' (range 0 LAST_PRINTABLE_ASCII)
                                (fun glyph (do (def! offset (+ LAST_ASCII 1))
				               (copy_glyph! from: font,glyph
			                                    to:   (+ glyph offset) ) )) )) )

(def! scroll_all_glyphs_to_the_right!
		(fun font (map' font scroll_glyph_right!)) )

(def! invert_all_glyphs_in_upper_half!
		(fun font
		     font | (filter (fun glyph (> glyph.id LAST_ASCII)))
                          | (map invert_glyph!) ) )				

;; Only the last expression of the do-expression will be returned to the caller
;; and therefore be evaluated eagerly. Hence we need all other functions to use
;; the eager version of map, which is map'. Otherwise these intermediate resul-
;; ts will not be evaluated. 
(def! prep_font! (fun [src_font misc_font dest_font]
                      (do (copy_umlauts! src_font dest_font)
		          (copy_special_glyphs! misc_font dest_font)
			  (copy_printable_ascii_glyphs! src_font dest_font)
		          (duplicate_ascii_glyphs_to_upper_half! dest_font)
		          (scroll_all_glyphs_to_the_right! dest_font)
			  (invert_all_glyphs_in_upper_half! dest_font) ) ))

;; ----------------------------------------------------------------------------
;; Load original fonts
;; ----------------------------------------------------------------------------

(def! serif_original    (load_font! "holgers_serif_cp850_08x16.fonted"))
(def! sans_original     (load_font! "holgers_sans_serif_cp850_08x16.fonted"))
(def! computer_original (load_font! "holgers_computer_cp850_08x16.fonted"))
(def! template_original (load_font! "holgers_template_cp850_08x16.fonted"))
(def! misc_original     (load_font! "miscellaneous_08x16.fonted"))

;; ----------------------------------------------------------------------------
;; Build and save derived fonts
;; ----------------------------------------------------------------------------

(def! serif_derived (new_font glyph_count: 256 glyph_width: 8 glyph_height: 16
                              name: "Holger's Serif Font" author: "Holger Zahnleiter"
                              codepage: "ASCII" description: "Font for serial ASCII terminal" ))
(prep_font! serif_original misc_original serif_derived)
(save_font! serif_derived "serif_derived.fonted")		

(def! sans_derived (new_font glyph_count: 256 glyph_width: 8 glyph_height: 16
                             name: "Holger's Sans Serif Font" author: "Holger Zahnleiter"
                             codepage: "ASCII" description: "Font for serial ASCII terminal" ))
(prep_font! sans_original misc_original sans_derived)
(save_font! sans_derived "sans_derived.fonted")					 

(def! computer_derived (new_font glyph_count: 256 glyph_width: 8 glyph_height: 16
                                 name: "Holger's Computer Font" author: "Holger Zahnleiter"
                                 codepage: "ASCII" description: "Font for serial ASCII terminal" ))
(prep_font! computer_original misc_original computer_derived)
(save_font! computer_derived "computer_derived.fonted")	

(def! template_derived (new_font glyph_count: 256 glyph_width: 8 glyph_height: 16
                                 name: "Holger's Template Font" author: "Holger Zahnleiter"
                                 codepage: "ASCII" description: "Font for serial ASCII terminal" ))
(prep_font! template_original misc_original template_derived)
(save_font! template_derived "template_derived.fonted")	

;; ----------------------------------------------------------------------------
;; Merge and export fonts
;; ----------------------------------------------------------------------------

(def! all_fonts (merged_font serif_derived sans_derived computer_derived template_derived))
(save_font! all_fonts "all_derived.fonted")
(export_binary_font! all_fonts "fonts") ;; Adds .bin suffix to name
